#include <stdio.h>
#include <string.h>

#define MAX_POINTS 10000
#define PARTITION_SIZE 8

float tree_sum(float *x)
{
	float tmp0 = x[0] + x[1];
	float tmp1 = x[2] + x[3];
	float tmp2 = x[4] + x[5];
	float tmp3 = x[6] + x[7];
	float tmp01 = tmp0 + tmp1;
	float tmp23 = tmp2 + tmp3;

	return tmp01 + tmp23;
}

void var(float *x_in, int *num_points, float *variance_out) {
#pragma HLS INTERFACE m_axi depth=10000 port=x_in offset=slave bundle=gmem1
#pragma HLS INTERFACE m_axi depth=1 port=num_points offset=slave bundle=gmem2
#pragma HLS INTERFACE m_axi depth=1 port=variance_out offset=slave bundle=gmem3
#pragma HLS INTERFACE s_axilite port=x_in bundle=control
#pragma HLS INTERFACE s_axilite port=num_points bundle=control
#pragma HLS INTERFACE s_axilite port=variance_out bundle=control
#pragma HLS INTERFACE s_axilite port=return bundle=control

	float x[MAX_POINTS];
#pragma HLS ARRAY_PARTITION variable=x cyclic factor=4 dim=1
	float mean = 0.0;
	float variance = 0.0;

	float buf0[PARTITION_SIZE] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
	float buf1[PARTITION_SIZE] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
	float buf2[PARTITION_SIZE] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
	float buf3[PARTITION_SIZE] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

	memcpy(x, (float*) x_in, MAX_POINTS * sizeof(float));

	// Compute the average;
	int i = 0;
	int j = 0;
	MEAN: for (i=0, j=0; i<MAX_POINTS; i+=4, j++) {
#pragma HLS PIPELINE II=1
		buf0[j%PARTITION_SIZE] += x[i];
		buf1[j%PARTITION_SIZE] += x[i+1];
		buf2[j%PARTITION_SIZE] += x[i+2];
		buf3[j%PARTITION_SIZE] += x[i+3];
	}

	mean = tree_sum(buf0) + tree_sum(buf1) + tree_sum(buf2) + tree_sum(buf3);
	mean /= *num_points;

	// Put the buffers to zero;
	REINIT_BUF: for (i=0; i<PARTITION_SIZE; i++) {
#pragma HLS PIPELINE II=1
		buf0[i] = 0;
		buf1[i] = 0;
		buf2[i] = 0;
		buf3[i] = 0;
	}

	// Compute the variance;
	VARIANCE: for (i=0, j=0; i<MAX_POINTS; i+=4, j++) {
#pragma HLS PIPELINE II=1
		buf0[j%PARTITION_SIZE] += x[i] * x[i];
		buf1[j%PARTITION_SIZE] += x[i+1] * x[i+1];
		buf2[j%PARTITION_SIZE] += x[i+2] * x[i+2];
		buf3[j%PARTITION_SIZE] += x[i+3] * x[i+3];
	}
	variance = tree_sum(buf0) + tree_sum(buf1) + tree_sum(buf2) + tree_sum(buf3);
	*variance_out = (variance - *num_points * (mean*mean)) / (*num_points - 1);
}
