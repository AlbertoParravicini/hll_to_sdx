#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <CL/opencl.h>
#include <time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>


////////////////////////////////////////////////////////////////////////////////


#define NUM_POINTS 10000


////////////////////////////////////////////////////////////////////////////////


float var_sw(float *x, int num_points) {
	int i = 0;
	float mean = 0.0;
	float variance = 0.0;

	for (i = 0; i < num_points; i++) {
		mean += x[i];
	}
	mean /= num_points;

	for (i = 0; i < num_points; i++) {
		variance += (x[i] - mean) * (x[i] - mean);
	}

	return variance / (num_points - 1);
}


////////////////////////////////////////////////////////////////////////////////


int load_file_to_memory(const char *filename, char **result) {
	size_t size = 0;
	FILE *f = fopen(filename, "rb");
	if (f == NULL) {
		*result = NULL;
		return -1; // -1 means file opening fail
	}
	fseek(f, 0, SEEK_END);
	size = ftell(f);
	fseek(f, 0, SEEK_SET);
	*result = (char *) malloc(size + 1);
	if (size != fread(*result, sizeof(char), size, f)) {
		free(*result);
		return -2; // -2 means file reading fail
	}
	fclose(f);
	(*result)[size] = 0;
	return size;
}


////////////////////////////////////////////////////////////////////////////////


int main(int argc, char** argv)
{
	//TARGET_DEVICE macro needs to be passed from gcc command line
#if defined(SDA_PLATFORM) && !defined(TARGET_DEVICE)
#define STR_VALUE(arg)      #arg
#define GET_STRING(name) STR_VALUE(name)
#define TARGET_DEVICE GET_STRING(SDA_PLATFORM)
#endif

	char *TARGET_DEVICES[] = {"xilinx:adm-pcie-ku3:2ddr-xpr:4.0", "xilinx_adm-pcie-7v3_1ddr_3_0", "xilinx_adm-pcie-ku3_2ddr-xpr_4_0"};
	char *XCLBIN_FILES[] = {"../kernel_ku3.xclbin", "../kernel_7v3.xclbin", "../kernel_ku3.xclbin"};
	int NUM_SUPPORTED_DEVICES = sizeof(TARGET_DEVICES) / sizeof(char *);
	char *xclbin;

	// FIFOs in the /tmp/ folder;
    const char *myfifo_in = "/tmp/myfifo_in";
    const char *myfifo_out = "/tmp/myfifo_out";

	int err;                            // error code returned from api calls

	// TODO: taken from input data;
	float variance;             		// results returned from device
	float variance_sw;          		// results returned from software
	int num_points = 0;

	cl_platform_id platforms[16];       // platform id
	cl_platform_id platform_id;         // platform id
	cl_uint platform_count;
	cl_device_id device_id;             // compute device id
	cl_context context;                 // compute context
	cl_command_queue commands;          // compute command queue
	cl_program program;                 // compute program
	cl_kernel kernel;                   // compute kernel

	char cl_platform_vendor[1001];

	// TODO: taken from input data (input arrays and output);
	cl_mem x_cl;                     // device memory used for the input array
	cl_mem num_points_cl;
	cl_mem output;                      // device memory used for the output array


	// Fill our data set with random values
	// TODO: to be changed with empty initialization of buffer, or values from input;

    // Opening connections to the FIFOs;
    printf("RECEIVER - OPENING CONNECTIONS...\n");
    int fifo_in = open(myfifo_in, O_RDONLY);
    int fifo_out = open(myfifo_out, O_WRONLY);

    printf("READING - RECEIVER...\n");
	ssize_t res_trans;

	// Read the input size;
    res_trans = read(fifo_in, &num_points, sizeof(int));

	// Read the input data;
    float *x = (float *) calloc(num_points, sizeof(float));
    res_trans = read(fifo_in, x, sizeof(float) * num_points);

	int i = 0;
	for (i = 0; i < num_points; i++)
	{
		printf("x[%d]=%f\n", i, x[i]);
	}


	// Get all platforms and then select Xilinx platform
	err = clGetPlatformIDs(16, platforms, &platform_count);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to find an OpenCL platform!\n");
		printf("Test failed with err: %d\n", err);
		return EXIT_FAILURE;
	}
	printf("INFO: Found %d platforms\n", platform_count);


	// Find Xilinx Plaftorm
	int platform_found = 0;
	for (unsigned int iplat = 0; iplat < platform_count; iplat++)
	{
		err = clGetPlatformInfo(platforms[iplat], CL_PLATFORM_VENDOR, 1000, (void *) cl_platform_vendor, NULL);

		if (err != CL_SUCCESS)
		{
			printf("Error: clGetPlatformInfo(CL_PLATFORM_VENDOR) failed!\n");
			return EXIT_FAILURE;
		}
		if (strcmp(cl_platform_vendor, "Xilinx") == 0)
		{
			printf("INFO: Selected platform %d from %s\n", iplat, cl_platform_vendor);
			platform_id = platforms[iplat];
			platform_found = 1;
			break;
		}
	}
	if (!platform_found)
	{
		printf("ERROR: Platform Xilinx not found. Exit.\n");
		return EXIT_FAILURE;
	}


	// Connect to a compute device.
	// Find all devices and then select the target device
	cl_device_id devices[16];  // compute device id
	cl_uint device_count;
	unsigned int device_found = 0;
	char cl_device_name[1001];
	err = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_ACCELERATOR, 16, devices, &device_count);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to create a device group!\n");
		return EXIT_FAILURE;
	}


	// Iterate all devices to select the target device.
	for (uint i = 0; i < device_count; i++)
	{
		err = clGetDeviceInfo(devices[i], CL_DEVICE_NAME, 1024, cl_device_name,	0);

		if (err != CL_SUCCESS)
		{
			printf("Error: Failed to get device name for device %d!\n", i);
			return EXIT_FAILURE;
		}
		printf("INFO: Analyzing device: %s\n", cl_device_name);
		for (int t = 0; t < NUM_SUPPORTED_DEVICES; t++)
		{
			if (strcmp(cl_device_name, TARGET_DEVICES[t]) == 0)
			{
				device_id = devices[i];
				device_found = 1;
				xclbin = XCLBIN_FILES[t];
				printf("INFO: Selected %s as the target device\n", cl_device_name);
				break;
			}
		}
	}
	if (!device_found) {
		printf("ERROR: Target device not found. Exit.\n");
		return EXIT_FAILURE;
	}


	// Create OpenCL context
	context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
	if (!context) {
		printf("Error: Failed to create a compute context!\n");
		return EXIT_FAILURE;
	}


	// Create Command Queue
	commands = clCreateCommandQueue(context, device_id, 0, &err);
	if (!commands)
	{
		printf("Error: Failed to create a command commands!\n");
		printf("Error: code %i\n", err);
		return EXIT_FAILURE;
	}


	// Create Program Objects
	int status;

	// Load binary from disk
	unsigned char *kernelbinary;
	printf("INFO: Loading %s\n", xclbin);
	int n_i = load_file_to_memory(xclbin, (char **) &kernelbinary);
	if (n_i < 0)
	{
		printf("failed to load kernel from xclbin: %s\n", xclbin);
		return EXIT_FAILURE;
	}

	size_t n = n_i;
	// Create the compute program from offline
	program = clCreateProgramWithBinary(context, 1, &device_id, &n,	(const unsigned char **) &kernelbinary, &status, &err);
	if ((!program) || (err != CL_SUCCESS))
	{
		printf("Error: Failed to create compute program from binary %d!\n",	err);
		return EXIT_FAILURE;
	}


	// Create the compute kernel in the program we wish to run
	// TODO: name from input;
	kernel = clCreateKernel(program, "var", &err);
	if (!kernel || err != CL_SUCCESS)
	{
		printf("Error: Failed to create compute kernel!\n");
		return EXIT_FAILURE;
	}


	// Create the input and output arrays in device memory for our calculation (input arrays, and output)
	// TODO: taken from input;
	x_cl = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(float) * NUM_POINTS, NULL, NULL);
	num_points_cl = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(int), NULL, NULL);
	output = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(float), NULL, NULL);


	// Write our data set into the input array in device memory (only arrays);
	// TODO: values from input;
	err = clEnqueueWriteBuffer(commands, x_cl, CL_TRUE, 0, sizeof(float) * NUM_POINTS, x, 0, NULL, NULL);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to write to input!\n");
		return EXIT_FAILURE;
	}
	err = clEnqueueWriteBuffer(commands, num_points_cl, CL_TRUE, 0, sizeof(int), &num_points, 0, NULL, NULL);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to write to input!\n");
		return EXIT_FAILURE;
	}


	// Set the arguments to our compute kernel (for all arguments, scalar and arrays);
	// TODO: taken from input
	err = 0;
	err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &x_cl);
	err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &num_points_cl);
	err |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &output);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to set kernel arguments! %d\n", err);
		return EXIT_FAILURE;
	}


	// Execute the kernel
	err = clEnqueueTask(commands, kernel, 0, NULL, NULL);


	// Read back the results from the device to verify the output
	cl_event readevent;
	err = clEnqueueReadBuffer(commands, output, CL_TRUE, 0, sizeof(float), &variance, 0, NULL, &readevent);
	if (err != CL_SUCCESS)
	{
		printf("Error: Failed to read output! %d\n", err);
		return EXIT_FAILURE;
	}


	clWaitForEvents(1, &readevent);


	printf("--> RECEIVER - VARIANCE=%f\n", variance);

	// Validate our results
	variance_sw = var_sw(x, NUM_POINTS);
	printf("--> RECEIVER - VARIANCE_SW=%f\n", variance_sw);


	// Shutdown and cleanup
	clReleaseMemObject(x_cl);
	clReleaseMemObject(output);
	clReleaseProgram(program);
	clReleaseKernel(kernel);
	clReleaseCommandQueue(commands);
	clReleaseContext(context);


	// Write to the output FIFO;
	printf("WRITING - RECEIVER...\n");
    res_trans = write(fifo_out, &variance, sizeof(float));

    printf("--> RECEIVER TRANSFER RESULT=%li", res_trans);

    // ===== CLOSE CONNECTION =====
    close(fifo_in);
    close(fifo_out);

    printf("\nStreams closed.\n");
}
