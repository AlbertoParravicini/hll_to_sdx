#ifndef MKFIFO_HOST_H_
#define MKFIFO_HOST_H_

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

////////////////////////////////////////////////////////////////////////////////

float var_sw(float *x, int num_points) {
	int i = 0;
	float mean = 0.0;
	float variance = 0.0;

	for (i = 0; i < num_points; i++) {
		mean += x[i];
	}
	mean /= num_points;

	for (i = 0; i < num_points; i++) {
		variance += (x[i] - mean) * (x[i] - mean);
	}

	return variance / (num_points - 1);
}

////////////////////////////////////////////////////////////////////////////////

void fifo_host(int num_points, float *x_in, float *variance) 
{
    
    // LINK FIFO IN TMP FOLDER
    const char *myfifo_in = "/tmp/myfifo_in";
    const char *myfifo_out = "/tmp/myfifo_out";

    printf("CREATING FIFOS...\n");
    mkfifo(myfifo_in, 0666);
    mkfifo(myfifo_out, 0666);

    // ===== OPEN CONNECTION =====
    printf("OPENING CONNECTIONS...\n");
    int fifo_in = open(myfifo_in, O_WRONLY);
    int fifo_out = open(myfifo_out, O_RDONLY);
    
    // ===== SEND DATA =====
    ssize_t res_trans;
    printf("WRITING - HOST...\n");
    // Write the random signal on the FIFO;
    res_trans = write(fifo_in, &num_points, sizeof(int));
    res_trans = write(fifo_in, x_in, sizeof(float) * num_points);

    // ===== RECEIVE DATA =====
    printf("READING - HOST...\n");
    res_trans = read(fifo_out, variance, sizeof(float));

    printf("--> HOST TRANSFER RESULT=%li", res_trans);

    // ===== CLOSE CONNECTION =====
    close(fifo_in);
    close(fifo_out);
    
    // UNLINK THE FIFO
    unlink(myfifo_in);
    unlink(myfifo_out);
    printf("\nStreams closed.\n");

}

int main()
{

    int num_points = 10000;
    float *x_in = (float *) calloc(num_points, sizeof(float)); 
    float variance_fifo = 0.0;
    float variance_gold = 0.0;

    // Create a random signal;
    int i = 0;
	for (i=0; i < num_points; i++)
	{
		x_in[i] = (float) rand() / (float) RAND_MAX;
		printf("x_in[%d]=%f\n", i, x_in[i]);
	}

    variance_gold = var_sw(x_in, num_points);

    // Send the data and get them back;
    printf("Sending data and wait for results...\n");
    fifo_host(num_points, x_in, &variance_fifo);

    // Print the result;
    printf("variance_gold=%f -- variance_fifo=%f\n", variance_gold, variance_fifo);

    free(x_in);
    return 0;
}

#endif