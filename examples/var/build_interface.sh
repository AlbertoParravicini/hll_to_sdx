#!/bin/bash
python3 ../../src/host_builder/host_builder.py -i ./src/config.json -p ../../src/host_builder/prototype.cpp -o ./src/host.cpp; 
python3 ../../src/fifo_interface_builder/fifo_interface_builder.py -i ./src/config.json -p ../../src/fifo_interface_builder/prototype.hpp -o ./build/fifo_interface.hpp;
# R
mkdir -p build/R;
python3 ../../src/r_builder/r_function_builder.py -i ./src/config.json -p  ../../src/r_builder/prototype_function.r -o ./build/R/function.R; 
python3 ../../src/r_builder/r_interface_builder.py -i ./src/config.json -p  ../../src/r_builder/prototype_interface.cpp -o ./build/R/r_interface.cpp;
# MATLAB
mkdir -p build/matlab; 
python3 ../../src/matlab_builder/matlab_function_builder.py -i ./src/config.json -p  ../../src/matlab_builder/prototype_function.m -o ./build/matlab/function.m; 
python3 ../../src/matlab_builder/matlab_interface_builder.py -i ./src/config.json -p  ../../src/matlab_builder/prototype_interface.cpp -o ./build/matlab/matlab_interface.cpp;
# Python
mkdir -p build/python;
python3 ../../src/python_builder/python_interface_builder.py -i ./src/config.json -p  ../../src/python_builder/prototype_interface.cpp -o ./build/python/python_interface.cpp;
cp ../../src/python_builder/function.py ./build/python;
cd ./build/python;
python3 ./function.py build;
mv ./build/lib.linux-x86_64-3.6/python_interface.cpython-36m-x86_64-linux-gnu.so ./python_interface.so;
rm -rf ./build/;
cd ../..;
