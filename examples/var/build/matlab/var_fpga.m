function [variance] = var_fpga(x, num_points)
    %mex matlab_interface.cpp
    [variance] = matlab_interface(x, num_points);
end
