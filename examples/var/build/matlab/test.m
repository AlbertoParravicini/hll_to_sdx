mex matlab_interface.cpp

for i=1:100
	
    a = rand(1,524288);

    tic
    var_sw = var(a);
    exec_time_sw = toc;

    tic
    var_hw = var_fpga(a, 524288);
    exec_time_hw = toc;

    file_path = '../../../../data/var_overall.csv';
    if exist(file_path, 'file')==2
      fid = fopen(file_path, 'a'); % open exist file and append contents
    else
      fid= fopen(file_path, 'w'); % create file and write to it
    end

    fprintf(fid, "var, matlab, overall_hw, %f\n", exec_time_hw);
    fprintf(fid, "var, matlab, overall_sw, %f\n", exec_time_sw);
    sprintf("var, matlab, overall_hw, %f\n", exec_time_hw)
    sprintf("var, matlab, overall_sw, %f\n", exec_time_sw)
end

fclose(fid);
