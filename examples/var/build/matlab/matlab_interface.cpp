#include "../fifo_interface.hpp"
#include "mex.h"
#include <sys/time.h>
#include <iostream>
#include <ctime>
#include <fstream>

/* TODO: Auto generate params */
void matlab_sender_interface(double *x, double *num_points, double *variance)
{

    /* Index used to cast arguments, if required;*/
    int i = 0;


    /* Cast the input arguments, if required;*/
    float *x_cast = (float *) malloc(524288 * sizeof(float));
    for(i=0; i<524288; i++)
    {
        x_cast[i] = (float) x[i];
    }
    int num_points_cast = (int) *num_points;


    /* Allocate the output arguments;*/
    float *variance_cast = (float *) malloc(sizeof(float));


    /* Call the FPGA;*/
    fifo_interface(x_cast, num_points_cast, variance_cast);


    /* Cast the output of the FPGA to doubles, to be used in MATLAB;*/
    *variance = (double) *variance_cast;


    /* Free the memory*/
    free(x_cast);
}


/* The gateway function */
void mexFunction(
    int nlhs,               /* Number of output (left-side) arguments, or the size of the plhs array. */
    mxArray *plhs[],        /* Array of output arguments. */
    int nrhs,               /* Number of input (right-side) arguments, or the size of the prhs array. */
    const mxArray *prhs[]   /* Array of input arguments. */
)
{

    /* Check for the proper number of arguments */
    if(nrhs!=2)
    {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:nrhs","Wrong number of inputs.");
    }
    if(nlhs!=1)
    {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:nlhs","Wrong number of outputs.");
    }


    /* Check the type of the parameters, we assume that they are double;
    Arrays are required to be row vectors;*/
    if( !mxIsScalar(prhs[1]) ||
            mxIsComplex(prhs[1]) ||
            mxGetNumberOfElements(prhs[1])!=1 )
    {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notScalar","The input argument must be a scalar.");
    }

    if (mxIsComplex(prhs[0]))
    {
        mexErrMsgTxt("The input must be a real array.");
    }
    if (mxIsSparse(prhs[0]))
    {
        mexErrMsgTxt("The input must be a dense array.");
    }
    if(mxGetM(prhs[0])!=1)
    {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notRowVector","The input must be a row vector.");
    }

    /* Get the value of each input;*/
    double num_points = mxGetScalar(prhs[1]);

    double *x = mxGetPr(prhs[0]);


    /* Create the output variables;*/
    plhs[0] = mxCreateNumericMatrix(1, 1, mxDOUBLE_CLASS, mxREAL);


    /* Get a pointer to the real data in the output matrix */
    double *variance = mxGetPr(plhs[0]);


    /* Call the computational routine */
    matlab_sender_interface(x, &num_points, variance);
}
