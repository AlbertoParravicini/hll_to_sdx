#include <stdlib.h>
#include <Rcpp.h>
#include "../fifo_interface.hpp"

using namespace Rcpp;

// [[Rcpp::export]]
double r_fifo_interface(NumericVector x, int num_points)
{

    // Index used to cast inputs, if required;
    int i = 0;


    // Cast the inputs, if required;
    float *x_cast = (float *) malloc(524288 * sizeof(float));
    for(i=0; i<524288; i++)
    {
        x_cast[i] = (float) x[i];
    }
    int num_points_cast = (int) num_points;


    // Allocate memory to store the output;
    float *variance_cast = (float *) malloc(sizeof(float));


    // Call the FPGA;
    fifo_interface(x_cast, num_points_cast, variance_cast);


    // Write the output;
    double variance = (double) *variance_cast;


    // Free the memory;
    free(x_cast);


    return variance;
}


/*** R
print("r_sender_interface imported correctly!")
*/