import python_interface
import numpy as np
import time

for i in range(100):
	x = [i.item() for i in np.random.uniform(size=524288)]

	start_time = time.time()
	var_hw = python_interface.var_fpga(x, len(x))
	exec_time_hw = time.time() - start_time
	print("--> VAR OVERALL HW TIME: {} seconds ---".format(exec_time_hw))
	print("var:", var_hw)


	start_time = time.time()
	var_sw = np.var(x)
	exec_time_sw = time.time() - start_time

	print("--> VAR OVERALL SW TIME: {} seconds ---".format(exec_time_sw))

	file_path = "../../../../data/var_overall.csv"

	with open(file_path, "a+") as f:
		f.write("var, python, overall_hw, {}\n".format(exec_time_hw))
		f.write("var, python, overall_sw, {}\n".format(exec_time_sw))
