#include <stdlib.h>
#include <boost/python/module.hpp>
#include <boost/python/def.hpp>
#include <boost/python.hpp>
#include <iostream>
#include "../fifo_interface.hpp"

using namespace boost::python;


template <class T>
static list to_python_list(T *in_vec, int length)
{
    int i = 0;
    list out_vec;

    for (i=0; i<length; i++)
    {
        out_vec.append(in_vec[i]);
    }
    return out_vec;
}


float var_fpga(const list x, const int num_points)
{

    // Index used to cast inputs, if required;
    int i=0;


    // Cast the inputs, if required;
    float *x_cast = (float *) malloc(524288 * sizeof(float));
    for(i=0; i<524288; i++)
    {
        x_cast[i] = extract<float>(x[i]);
    }
    int num_points_cast = (int) num_points;


    // Allocate memory to store the output;
    float *variance_cast = (float *) malloc(sizeof(float));


    fifo_interface(x_cast, num_points_cast, variance_cast);


    // Free the memory;
    free(x_cast);


    return *variance_cast;
}


BOOST_PYTHON_MODULE(python_interface)
{
    def("var_fpga", var_fpga);
}
