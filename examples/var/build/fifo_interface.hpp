#ifndef MKFIFO_HOST_H_
#define MKFIFO_HOST_H_

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>
#include <iostream>
#include <ctime>
#include <fstream>

void fifo_interface(float *x, int num_points, float *variance)
{
    // Opening connections;
    int fifo_in = open("/tmp/myfifo_in", O_WRONLY);
    int fifo_out = open("/tmp/myfifo_out", O_RDONLY);


    //Sending data to the FIFOs;
    ssize_t res_trans;

    // Write the inputs on the FIFO;

    
    res_trans = write(fifo_in, x, sizeof(float) * 524288);
    res_trans = write(fifo_in, &num_points, sizeof(int));
    
    // Read the outputs from the FIFOs;
    res_trans = read(fifo_out, variance, sizeof(float));

    // Close the FIFOs;
    close(fifo_in);
    close(fifo_out);
}

#endif
