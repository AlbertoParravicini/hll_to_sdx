#ifndef MKFIFO_HOST_H_
#define MKFIFO_HOST_H_

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>
#include <iostream>
#include <ctime>
#include <fstream>

void fifo_interface(int *a, int *b, int *c)
{
    // Opening connections;
    int fifo_in = open("/tmp/myfifo_in", O_WRONLY);
    int fifo_out = open("/tmp/myfifo_out", O_RDONLY);

    // Used to measure execution time;
    timespec ts_start;
    timespec ts_end;

    //Sending data to the FIFOs;
    ssize_t res_trans;

    // Write the inputs on the FIFO;


    res_trans = write(fifo_in, a, sizeof(int) * 262144);
    res_trans = write(fifo_in, b, sizeof(int) * 262144);
  

    // Read the outputs from the FIFOs;
    res_trans = read(fifo_out, c, sizeof(int) * 262144);

    // Close the FIFOs;
    close(fifo_in);
    close(fifo_out);
}

#endif
