function [c] = mmult_fpga(a, b)
    mex -v COMPFLAGS='$COMPFLAGS -rtl' matlab_interface.cpp
    [c] = matlab_interface(a, b);
end
