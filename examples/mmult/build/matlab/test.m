mex matlab_interface.cpp
for i=1:100
	
    NUM_POINTS = 512;
    a = randi(100,1,NUM_POINTS*NUM_POINTS);
    b = randi(100,1,NUM_POINTS*NUM_POINTS);

    a1 = reshape(a, [NUM_POINTS,NUM_POINTS])';
    b1 = reshape(b, [NUM_POINTS, NUM_POINTS])';

    tic
    c1 = a1*b1;
    exec_time_sw = toc;

    tic
    c = mmult_fpga(a, b);
    exec_time_hw = toc;

    file_path = '../../../../data/mmult_overall.csv';
    if exist(file_path, 'file')==2
      fid = fopen(file_path, 'a'); % open exist file and append contents
    else
      fid= fopen(file_path, 'w'); % create file and write to it
    end

    fprintf(fid, "mmult, matlab, overall_hw, %f\n", exec_time_hw);
    fprintf(fid, "mmult, matlab, overall_sw, %f\n", exec_time_sw);
    sprintf("mmult, matlab, overall_hw, %f\n", exec_time_hw)
    sprintf("mmult, matlab, overall_sw, %f\n", exec_time_sw)

    fclose(fid);
end
