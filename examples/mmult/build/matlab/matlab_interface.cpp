#include "../fifo_interface.hpp"
#include "mex.h"
#include <sys/time.h>
#include <iostream>
#include <ctime>
#include <fstream>

/* TODO: Auto generate params */
void matlab_sender_interface(double *a, double *b, double *c)
{

    /* Index used to cast arguments, if required;*/
    int i = 0;


    /* Cast the input arguments, if required;*/
    int *a_cast = (int *) malloc(262144 * sizeof(int));
    for(i=0; i<262144; i++)
    {
        a_cast[i] = (int) a[i];
    }
    int *b_cast = (int *) malloc(262144 * sizeof(int));
    for(i=0; i<262144; i++)
    {
        b_cast[i] = (int) b[i];
    }


    /* Allocate the output arguments;*/
    int *c_cast = (int *) malloc(262144 * sizeof(int));


    /* Call the FPGA;*/
    fifo_interface(a_cast, b_cast, c_cast);


    /* Cast the output of the FPGA to doubles, to be used in MATLAB;*/
    for(i=0; i<262144; i++)
    {
        c[i] = (double) c_cast[i];
    }


    /* Free the memory*/
    free(a_cast);
    free(b_cast);
    free(c_cast);
}


/* The gateway function */
void mexFunction(
    int nlhs,               /* Number of output (left-side) arguments, or the size of the plhs array. */
    mxArray *plhs[],        /* Array of output arguments. */
    int nrhs,               /* Number of input (right-side) arguments, or the size of the prhs array. */
    const mxArray *prhs[]   /* Array of input arguments. */
)
{

    /* Check for the proper number of arguments */
    if(nrhs!=2)
    {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:nrhs","Wrong number of inputs.");
    }
    if(nlhs!=1)
    {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:nlhs","Wrong number of outputs.");
    }


    /* Check the type of the parameters, we assume that they are double;
    Arrays are required to be row vectors;*/

    if (mxIsComplex(prhs[0]))
    {
        mexErrMsgTxt("The input must be a real array.");
    }
    if (mxIsSparse(prhs[0]))
    {
        mexErrMsgTxt("The input must be a dense array.");
    }
    if(mxGetM(prhs[0])!=1)
    {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notRowVector","The input must be a row vector.");
    }

    if (mxIsComplex(prhs[1]))
    {
        mexErrMsgTxt("The input must be a real array.");
    }
    if (mxIsSparse(prhs[1]))
    {
        mexErrMsgTxt("The input must be a dense array.");
    }
    if(mxGetM(prhs[1])!=1)
    {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notRowVector","The input must be a row vector.");
    }

    /* Get the value of each input;*/

    double *a = mxGetPr(prhs[0]);
    double *b = mxGetPr(prhs[1]);


    /* Create the output variables;*/
    plhs[0] = mxCreateNumericMatrix(1, 262144, mxDOUBLE_CLASS, mxREAL);


    /* Get a pointer to the real data in the output matrix */
    double *c = mxGetPr(plhs[0]);


    /* Call the computational routine */
    matlab_sender_interface(a, b, c);
}
