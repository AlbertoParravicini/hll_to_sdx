#include <stdlib.h>
#include <Rcpp.h>
#include "../fifo_interface.hpp"

using namespace Rcpp;

// [[Rcpp::export]]
IntegerVector r_fifo_interface(IntegerVector a, IntegerVector b)
{

    // Index used to cast inputs, if required;
    int i = 0;


    // Cast the inputs, if required;
    int *a_cast = (int *) malloc(262144 * sizeof(int));
    for(i=0; i<262144; i++)
    {
        a_cast[i] = (int) a[i];
    }
    int *b_cast = (int *) malloc(262144 * sizeof(int));
    for(i=0; i<262144; i++)
    {
        b_cast[i] = (int) b[i];
    }


    // Allocate memory to store the output;
    int *c_cast = (int *) malloc(262144 * sizeof(int));


    // Call the FPGA;
    fifo_interface(a_cast, b_cast, c_cast);


    // Write the output;
    IntegerVector c(262144);
    for(i=0; i<262144; i++)
    {
        c[i] = (int) c_cast[i];
    }


    // Free the memory;
    free(a_cast);
    free(b_cast);
    free(c_cast);


    return c;
}


/*** R
print("r_sender_interface imported correctly!")
*/