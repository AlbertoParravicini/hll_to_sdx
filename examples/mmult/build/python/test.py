import python_interface
import numpy as np
import time
import pandas

for i in range(100):
	a = [i.item() for i in np.random.randint(100, size=512*512)]
	b = [i.item() for i in np.random.randint(100, size=512*512)]

	start_time = time.time()
	c_hw = python_interface.mmult_fpga(a, b)
	exec_time_hw = time.time() - start_time
	print("--> MMULT OVERALL HW TIME: {} seconds ---".format(exec_time_hw))

	am = np.array(a).reshape((512, 512))
	bm = np.array(b).reshape((512, 512))

	start_time = time.time()
	c_sw = am.dot(bm)
	exec_time_sw = time.time() - start_time

	print("--> MMULT OVERALL SW TIME: {} seconds ---".format(exec_time_sw))

	file_path = "../../../../data/mmult_overall.csv"

	with open(file_path, "a+") as f:
		f.write("mmult, python, overall_hw, {}\n".format(exec_time_hw))
		f.write("mmult, python, overall_sw, {}\n".format(exec_time_sw))
