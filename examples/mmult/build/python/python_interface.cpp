#include <stdlib.h>
#include <boost/python/module.hpp>
#include <boost/python/def.hpp>
#include <boost/python.hpp>
#include <iostream>
#include "../fifo_interface.hpp"

using namespace boost::python;


template <class T>
static list to_python_list(T *in_vec, int length)
{
    int i = 0;
    list out_vec;

    for (i=0; i<length; i++)
    {
        out_vec.append(in_vec[i]);
    }
    return out_vec;
}


list mmult_fpga(const list a, const list b)
{

    // Index used to cast inputs, if required;
    int i=0;


    // Cast the inputs, if required;
    int *a_cast = (int *) malloc(262144 * sizeof(int));
    for(i=0; i<262144; i++)
    {
        a_cast[i] = extract<int>(a[i]);
    }
    int *b_cast = (int *) malloc(262144 * sizeof(int));
    for(i=0; i<262144; i++)
    {
        b_cast[i] = extract<int>(b[i]);
    }


    // Allocate memory to store the output;
    int *c_cast = (int *) malloc(262144 * sizeof(int));


    fifo_interface(a_cast, b_cast, c_cast);


    // Free the memory;
    free(a_cast);
    free(b_cast);


    return to_python_list(c_cast, 262144);
}


BOOST_PYTHON_MODULE(python_interface)
{
    def("mmult_fpga", mmult_fpga);
}
