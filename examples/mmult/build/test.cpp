#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>
#include <fstream>


int main()
{
    timespec ts_start;
    clock_gettime(CLOCK_REALTIME, &ts_start); // Works on Linux

    for (int i = 0; i < 1000000; i++)
    {
        int j = i * i;
    }
    timespec ts_end;
    clock_gettime(CLOCK_REALTIME, &ts_end);

    double exec_time = (double)(ts_end.tv_nsec - ts_start.tv_nsec) / 1000000000;
    std::cout << "EXEC TIME: " << exec_time << std::endl;

    std::ofstream output_file;
    output_file.open("./test.csv", std::ios_base::app);
    output_file << "test, test, test, " << exec_time  << std::endl;
    output_file.close();
}