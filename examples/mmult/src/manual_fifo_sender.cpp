#ifndef MKFIFO_HOST_H_
#define MKFIFO_HOST_H_

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>



////////////////////////////////////////////////////////////////////////////////


int main()
{
    int MATRIX_RANK = 16;
    int data_size = MATRIX_RANK * MATRIX_RANK;
    int num_points = 10000;
    int *a = (int *) calloc(data_size, sizeof(int)); 
    int *b = (int *) calloc(data_size, sizeof(int)); 
    int *c = (int *) calloc(data_size, sizeof(int)); 
    int correct = 0;

    // Create a random signal;
    int i = 0;
    for(i=0; i<data_size; i++) {
        a[i] = (int) i;
        b[i] = (int) i;
        c[i] = 0;
    }

    // LINK FIFO IN TMP FOLDER
    const char *myfifo_in = "/tmp/myfifo_in";
    const char *myfifo_out = "/tmp/myfifo_out";

    // ===== OPEN CONNECTION =====
    printf("OPENING CONNECTIONS...\n");
    int fifo_in = open(myfifo_in, O_WRONLY);
    int fifo_out = open(myfifo_out, O_RDONLY);
    
    // ===== SEND DATA =====
    ssize_t res_trans;
    printf("WRITING - HOST...\n");
    // Write the random signal on the FIFO;
    res_trans = write(fifo_in, a, sizeof(int) * data_size);
    res_trans = write(fifo_in, b, sizeof(int) * data_size);

    // ===== RECEIVE DATA =====
    printf("READING - HOST...\n");
    res_trans = read(fifo_out, c, sizeof(int) * data_size);

    printf("--> HOST TRANSFER RESULT=%li", res_trans);

    // ===== CLOSE CONNECTION =====
    close(fifo_in);
    close(fifo_out);
    

    // Print the result;
        printf("A\n");
    for (i=0; i<data_size; i++) {
        printf("%x ",a[i]);
        if (((i+1) % 16) == 0)
            printf("\n");
    }
    printf("B\n");
    for (i=0; i<data_size; i++) {
        printf("%x ",b[i]);
        if (((i+1) % 16) == 0)
            printf("\n");
    }
    printf("res\n");
    for (i=0; i<data_size; i++) {
        printf("%x ",c[i]);
        if (((i+1) % 16) == 0)
            printf("\n");
    }

    int sw_results[data_size];   
    correct = 0;
    for(i = 0; i < data_size; i++)
        {
            int row = i/MATRIX_RANK;
            int col = i%MATRIX_RANK;
            int running = 0;
            int index;
            for (index=0;index<MATRIX_RANK;index++) {
                int aIndex = row*MATRIX_RANK + index;
                int bIndex = col + index*MATRIX_RANK;
                running += a[aIndex] * b[bIndex];
            }
            sw_results[i] = running;
        }
    
    for (i = 0;i < data_size; i++) 
        if(c[i] == sw_results[i])
            correct++;
    printf("Software\n");
    for (i=0;i<data_size;i++) {
        printf("%d ",sw_results[i]);
        if (((i+1) % 16) == 0)
            printf("\n");
    }
    
    
    // Print a brief summary detailing the results
    //
    printf("Computed '%d/%d' correct values!\n", correct, data_size);

    free(a);
    free(b);
    free(c);

    return 0;
}

#endif