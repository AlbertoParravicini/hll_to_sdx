#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <CL/opencl.h>
#include <time.h>
#include <sys/stat.h>
#include <fcntl.h>    /* For O_RDWR, O_WRONLY */
#include <unistd.h>   /* For open(), read(), ... */
#include <sys/time.h>
#include <iostream>
#include <ctime>
#include <fstream>


////////////////////////////////////////////////////////////////////////////////


int load_file_to_memory(const char *filename, char **result)
{
    size_t size = 0;
    FILE *f = fopen(filename, "rb");
    if (f == NULL)
    {
        *result = NULL;
        return -1; // -1 means file opening fail
    }
    fseek(f, 0, SEEK_END);
    size = ftell(f);
    fseek(f, 0, SEEK_SET);
    *result = (char *) malloc(size + 1);
    if (size != fread(*result, sizeof(char), size, f))
    {
        free(*result);
        return -2; // -2 means file reading fail
    }
    fclose(f);
    (*result)[size] = 0;
    return size;
}


////////////////////////////////////////////////////////////////////////////////


int main(int argc, char** argv)
{
#if defined(SDA_PLATFORM) && !defined(TARGET_DEVICE)
#define STR_VALUE(arg)      #arg
#define GET_STRING(name) STR_VALUE(name)
#define TARGET_DEVICE GET_STRING(SDA_PLATFORM)
#endif

    // Used to measure execution time;
    timespec ts_start;
    timespec ts_end;
    //clock_gettime(CLOCK_REALTIME, &ts_start);

    char *TARGET_DEVICES[] = {"xilinx_adm-pcie-7v3_1ddr_3_0"};
    char *XCLBIN_FILES[] = {"kernel_7v3.xclbin"};
    int NUM_SUPPORTED_DEVICES = sizeof(TARGET_DEVICES) / sizeof(char *);
    char *xclbin;

    int err;                            // error code returned from api calls

    // Inputs;
    int *a;
    int *b;
    // Outputs;
    int *c;

    cl_platform_id platforms[16];       // platform id
    cl_platform_id platform_id;         // platform id
    cl_uint platform_count;
    cl_device_id device_id;             // compute device id
    cl_context context;                 // compute context
    cl_command_queue commands;          // compute command queue
    cl_program program;                 // compute program
    cl_kernel kernel;                   // compute kernel

    char cl_platform_vendor[1001];


    // Get all platforms and then select Xilinx platform
    err = clGetPlatformIDs(16, platforms, &platform_count);
    if (err != CL_SUCCESS)
    {
        printf("Error: Failed to find an OpenCL platform!\n");
        printf("Test failed with err: %d\n", err);
        return EXIT_FAILURE;
    }
    printf("INFO: Found %d platforms\n", platform_count);


    // Find Xilinx Plaftorm
    int platform_found = 0;
    for (unsigned int iplat = 0; iplat < platform_count; iplat++)
    {
        err = clGetPlatformInfo(platforms[iplat], CL_PLATFORM_VENDOR, 1000, (void *) cl_platform_vendor, NULL);

        if (err != CL_SUCCESS)
        {
            printf("Error: clGetPlatformInfo(CL_PLATFORM_VENDOR) failed!\n");
            return EXIT_FAILURE;
        }
        if (strcmp(cl_platform_vendor, "Xilinx") == 0)
        {
            printf("INFO: Selected platform %d from %s\n", iplat, cl_platform_vendor);
            platform_id = platforms[iplat];
            platform_found = 1;
            break;
        }
    }
    if (!platform_found)
    {
        printf("ERROR: Platform Xilinx not found. Exit.\n");
        return EXIT_FAILURE;
    }


    // Connect to a compute device.
    // Find all devices and then select the target device
    cl_device_id devices[16];  // compute device id
    cl_uint device_count;
    unsigned int device_found = 0;
    char cl_device_name[1001];
    err = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_ACCELERATOR, 16, devices, &device_count);
    if (err != CL_SUCCESS)
    {
        printf("Error: Failed to create a device group!\n");
        return EXIT_FAILURE;
    }


    // Iterate all devices to select the target device.
    for (uint i = 0; i < device_count; i++)
    {
        err = clGetDeviceInfo(devices[i], CL_DEVICE_NAME, 1024, cl_device_name,	0);

        if (err != CL_SUCCESS)
        {
            printf("Error: Failed to get device name for device %d!\n", i);
            return EXIT_FAILURE;
        }
        printf("INFO: Analyzing device: %s\n", cl_device_name);
        for (int t = 0; t < NUM_SUPPORTED_DEVICES; t++)
        {
            if (strcmp(cl_device_name, TARGET_DEVICES[t]) == 0)
            {
                device_id = devices[i];
                device_found = 1;
                xclbin = XCLBIN_FILES[t];
                printf("INFO: Selected %s as the target device\n", cl_device_name);
                break;
            }
        }
    }
    if (!device_found)
    {
        printf("ERROR: Target device not found. Exit.\n");
        return EXIT_FAILURE;
    }


    // Create OpenCL context
    context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
    if (!context)
    {
        printf("Error: Failed to create a compute context!\n");
        return EXIT_FAILURE;
    }


    // Create Command Queue
    commands = clCreateCommandQueue(context, device_id, 0, &err);
    if (!commands)
    {
        printf("Error: Failed to create a command commands!\n");
        printf("Error: code %i\n", err);
        return EXIT_FAILURE;
    }


    // Create Program Objects
    int status;

    // Load binary from disk
    unsigned char *kernelbinary;
    printf("INFO: Loading %s\n", xclbin);
    int n_i = load_file_to_memory(xclbin, (char **) &kernelbinary);
    if (n_i < 0)
    {
        printf("failed to load kernel from xclbin: %s\n", xclbin);
        return EXIT_FAILURE;
    }

    size_t n = n_i;
    // Create the compute program from offline
    program = clCreateProgramWithBinary(context, 1, &device_id, &n,	(const unsigned char **) &kernelbinary, &status, &err);
    if ((!program) || (err != CL_SUCCESS))
    {
        printf("Error: Failed to create compute program from binary %d!\n",	err);
        return EXIT_FAILURE;
    }


    // Create the compute kernel in the program we wish to run
    kernel = clCreateKernel(program, "mmult", &err);
    if (!kernel || err != CL_SUCCESS)
    {
        printf("Error: Failed to create compute kernel!\n");
        return EXIT_FAILURE;
    }



    // FIFOs in the /tmp/ folder;
    const char *myfifo_in = "/tmp/myfifo_in";
    const char *myfifo_out = "/tmp/myfifo_out";

    // Creating the FIFOs;
    printf("Receiver - Creating FIFOs...\n");
    mkfifo(myfifo_in, 0666);
    mkfifo(myfifo_out, 0666);

    // Opening connections to the FIFOs;
    printf("Receiver - Opening FIFOs...\n");
    int fifo_in = open(myfifo_in, O_RDWR);
    int fifo_out = open(myfifo_out, O_RDWR);

    clock_gettime(CLOCK_REALTIME, &ts_end);
    double exec_time = (double)(ts_end.tv_nsec - ts_start.tv_nsec) / 1000000000;
    std::cout << "--> FPGA SETUP TIME=" << exec_time << std::endl;

    // Store the execution time;
    std::ofstream output_file;
    output_file.open("../../../data/host.csv", std::ios_base::app);
    output_file << "mmult, , board_setup, " << exec_time  << std::endl;
    output_file.close();


    // Start the main kernel computation;
    while (true)
    {

        // Read values from the FIFOs;
        printf("--> Receiver - Reading input data...\n");
        ssize_t res_trans;

        // Kernel inputs;
        cl_mem a_cl;
        cl_mem b_cl;
        // Kernel outputs;
        cl_mem c_cl;


        // Allocate inputs and read them from the FIFOs;
        a = (int *) calloc(262144, sizeof(int));
        res_trans = read(fifo_in, a, sizeof(int) * 262144);
        b = (int *) calloc(262144, sizeof(int));
        res_trans = read(fifo_in, b, sizeof(int) * 262144);
        // Allocate output arrays, if present;
        c = (int *) calloc(262144, sizeof(int));

        // Measure memory transfer to the FPGA;
        clock_gettime(CLOCK_REALTIME, &ts_start);

        // Create the inputs and outputs in device memory for our calculation;
        a_cl = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(int) * 262144, NULL, NULL);
        if (!a_cl)
        {
            printf("Error: Failed to allocate device memory!\n");
            return EXIT_FAILURE;
        }
        b_cl = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(int) * 262144, NULL, NULL);
        if (!b_cl)
        {
            printf("Error: Failed to allocate device memory!\n");
            return EXIT_FAILURE;
        }
        c_cl = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(int) * 262144, NULL, NULL);
        if (!c_cl)
        {
            printf("Error: Failed to allocate device memory!\n");
            return EXIT_FAILURE;
        }


        // Write our inputs into the device memory;
        err = clEnqueueWriteBuffer(commands, a_cl, CL_TRUE, 0, sizeof(int) * 262144, a, 0, NULL, NULL);
        if (err != CL_SUCCESS)
        {
            printf("Error: Failed to write to queue!\n");
            return EXIT_FAILURE;
        }
        err = clEnqueueWriteBuffer(commands, b_cl, CL_TRUE, 0, sizeof(int) * 262144, b, 0, NULL, NULL);
        if (err != CL_SUCCESS)
        {
            printf("Error: Failed to write to queue!\n");
            return EXIT_FAILURE;
        }

        // Set the arguments to our compute kernel (for all arguments, scalar and arrays);
        err = 0;
        err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &a_cl);
        err = clSetKernelArg(kernel, 1, sizeof(cl_mem), &b_cl);
        err = clSetKernelArg(kernel, 2, sizeof(cl_mem), &c_cl);
        if (err != CL_SUCCESS)
        {
            printf("Error: Failed to set kernel arguments! %d\n", err);
            return EXIT_FAILURE;
        }

        clock_gettime(CLOCK_REALTIME, &ts_end);
        exec_time = (double)(ts_end.tv_nsec - ts_start.tv_nsec) / 1000000000;
        std::cout << "--> FPGA TRANSFER TIME=" << exec_time << std::endl;

        // Store the execution time;
        output_file.open("../../../data/host.csv", std::ios_base::app);
        output_file << "mmult, , fpga_input, " << exec_time  << std::endl;
        output_file.close();


        // Measure kernel execution time;
        clock_gettime(CLOCK_REALTIME, &ts_start);

        // Execute the kernel
        err = clEnqueueTask(commands, kernel, 0, NULL, NULL);

        clock_gettime(CLOCK_REALTIME, &ts_end);
        exec_time = (double)(ts_end.tv_nsec - ts_start.tv_nsec) / 1000000000;
        std::cout << "--> FPGA EXEC TIME=" << exec_time << std::endl;

        // Store the execution time;
        output_file.open("../../../data/host.csv", std::ios_base::app);
        output_file << "mmult, , board_exec, " << exec_time  << std::endl;
        output_file.close();

        // Read back the results from the device to verify the output
        cl_event readevent;
        err = clEnqueueReadBuffer(commands, c_cl, CL_TRUE, 0, sizeof(int) * 262144, c, 0, NULL, &readevent);
        if (err != CL_SUCCESS)
        {
            printf("Error: Failed to read output! %d\n", err);
            return EXIT_FAILURE;
        }

        clWaitForEvents(1, &readevent);

        // Measure output transfer time;
        clock_gettime(CLOCK_REALTIME, &ts_start);

        // Write values to the output FIFOs;
        // printf("--> Receiver  - Writing to output...\n");
        res_trans = write(fifo_out, c, sizeof(int) * 262144);

        clock_gettime(CLOCK_REALTIME, &ts_end);
        exec_time = (double)(ts_end.tv_nsec - ts_start.tv_nsec) / 1000000000;
        std::cout << "--> FIFO OUTPUT TIME=" << exec_time << std::endl;

        // Store the execution time;
        output_file.open("../../../data/host.csv", std::ios_base::app);
        output_file << "mmult, , fifo_output, " << exec_time  << std::endl;
        output_file.close();

        printf("--> Receiver - Transfer Result: %li\n", res_trans);



        // Release the OpenCL buffers;
        clReleaseMemObject(a_cl);
        clReleaseMemObject(b_cl);
        clReleaseMemObject(c_cl);

        // Free the memory;
        free(a);
        free(b);
        free(c);

    }


    // Release the remaining OpenCL variables;
    clReleaseProgram(program);
    clReleaseKernel(kernel);
    clReleaseCommandQueue(commands);
    clReleaseContext(context);


    // Close the FIFOs;
    close(fifo_in);
    close(fifo_out);
    printf("\n--> Receiver - Streams closed.\n");

    // Unlink the FIFOs;
    unlink(myfifo_in);
    unlink(myfifo_out);
    printf("\n--> Receiver - FIFOs unlinked\n");




    return 0;
}
