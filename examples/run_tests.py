import os

num_runs = 10

# Run mmult tests;
for i in range(num_runs):
	os.system("python ../mmult/build/python/test.py")
	os.system("matlab -nodisplay -nodesktop -r ../mmult/build/matlab/test.m")
	os.system("R < ../mmult/build/R/test.r --no-save")