***************************
Sum for the PYNQ platform
***************************

.. contents:: Table of Contents
   :depth: 3
   
This section will show you how to implement a sum between two integer for the PYNQ platform.


Vivado HLS
=================