.. numPYNQ documentation master file, created by
   sphinx-quickstart on Sun Mar 26 16:01:48 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
  
.. |logo| image:: ./img/numpynq_logo.png
    :width: 40pt
    :height: 40pt


|logo| numPYNQ: an efficient implementation of the NumPy library for the PYNQ platform. 
=======================================================================================


.. toctree::
   :maxdepth: 2
   
   src/001_intro
   src/002_sum_hw



.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

