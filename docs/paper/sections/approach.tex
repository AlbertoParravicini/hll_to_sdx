%!TEX root = ../paper.tex

\begin{figure}[H]
\centering
\includegraphics[width=0.8\columnwidth]{img/scheme.pdf}
\caption{Scheme of the proposed framework. The upper box represents the high-level language module, while the lower box represents the OpenCL/FPGA module. The two modules are connected by using FIFO buffers.}
\label{fig:scheme}
\end{figure}

\section{Proposed approach}
\label{sec:approach}

The main goal of our framework is to enable a user to automatically generate the interfaces of an OpenCL kernel to the most common high-level languages used in data science. We focused on Python, R, and MATLAB, which are often the languages of choice of data scientist, quantitative analysts and computational statisticians \cite{kdnuggets}.

From a description of the inputs and outputs of the kernel (Listing~\ref{lst:json}), in terms of structure (scalar or array) and type (int, float, etc\ldots), our framework is able to automatically generate all the interfaces required to connect the kernel to the high-level languages.  

We focused on \ac{fpga} C/C++ kernels written following the OpenCL paradigm, optimized by Vivado HLS, and compiled with the OpenCL compiler provided within the Xilinx SDAccel workflow. The framework also works with kernels written in \ac{hdl} like Verilog and VHDL, or with the Xilinx OpenCL libraries. 


Moreover, the framework can wrap kernels that behave as black-boxes, and for which only the input/output specifics are known (which is usually the case for kernels that are sold commercially).  
A strong point of our work is that it can be used to interface kernels that run either on CPUs, GPUs and FPGAs making it highly flexible to many different \ac{hsa} systems.

\begin{lstlisting}[caption={Example of kernel description},label={lst:json},language=json]
{
	"kernel_name": "mmult",
	"board": ["xilinx_adm-pcie-7v3_1ddr_3_0"],
	"xclbin": ["kernel_7v3.xclbin"],
	"num_iterations": 3,
	"inputs": [
	
			{"type": "array",
			"name": "a",
			"length": 256,
			"class": "int",
			"position": 0},
			
			{"type": "array",
			"name": "b",
			"length":256,
			"class": "int",
			"position": 1}],
			
	"outputs": [
	
			{"type": "array",
			"name": "c",
			"length":256,
			"class": "int",
			"position": 2}]
}

\end{lstlisting}

The structure of the framework can be divided into two main modules (Figure~\ref{fig:scheme}). The first module represents the OpenCL host interface, the software that controls the accelerator and the kernel itself. The second module is used to connect the high-level languages to the host interface, by using language specific libraries and tools. The two modules are connected by a streaming interface based on named pipes, created in Linux through \texttt{mkfifo}.

\subsection{Low-Level Module}

The low-level module of our framework is composed by the computational kernel which is wrapped in the user application, and by an OpenCL host file that manages the kernel. The kernel is compiled by an OpenCL compiler, and all the platforms supported by the OpenCL standard can be used.
The input/output specifics of the kernel are described with a small configuration file, that is used by the framework to build the interfaces.

The OpenCL runtime provided by the host file configures and manages the FPGA, and launches the kernel whenever the required data are available. The host file is unique and independent from the high-level language chosen by the user, and it requires recompilation only if the target accelerator or the target kernel are changed.

\subsection{High-Level Module}

The high-level module gives the user the ability to call the OpenCL kernel directly from Python, R or MATLAB, without having to manually configure the FPGA or handling the data transfer. The upper portion of the module is used to convert the data of high-level languages to the types and data structures that can be processed by OpenCL and by the FPGA kernel. This is accomplished by making use of different libraries, depending on the language that is considered.

In the case of Python, we make use of the Boost.Python\cite{abrahams2003building} library, which allows wrapping of C/C++ function and classes in modules that can be imported and invoked from Python. The structure of the module itself is independent from the target kernel, but arrays have to be converted from \texttt{boost::python::list} to standard C arrays before being sent to the OpenCL host.

R allows to compile and execute C++ functions through the Rcpp package \cite{rcpp}, and invoke them like traditional R functions. As in the case of Python, R data-types must be converted to regular C/C++ types (e.g. arrays of integers become IntegerVectors), both when sending and receiving data from the FPGA. The conversion is handled by R’s C interface \cite{autocorrelation_2}, which casts the subtypes of defined R data type to default C++ types.

In MATLAB, the interface with C is implemented through MEX files \cite{mex-api}. MEX files are dynamically linked subroutines executed by MATLAB as if they were built-in functions.
MATLAB is optimized to work on floating point numbers and doesn't offer full support to integer numbers. However, we can cast floating point numbers to integers if the OpenCL kernel demands so.

These modules convert the data to the appropriate data types, and then call a language-independent function that is connected to the OpenCL by the named pipes. This function will send and receive the data through the named pipes.\newline


The two modules are connected by using named pipes. Our framework uses one input and one output pipe, which are created and managed by the OpenCL host. After completing the FPGA reconfiguration, the host waits for data to be sent by an application on the input pipe, and will return the results to the output pipe. If desired, the user can require the host to run in a server-like mode, meaning that the host will remain active after having processed the data, so that new data can be sent and processed. This optimization allows to mask the FPGA reconfiguration time, and drastically reduce the execution time overheads for a kernel that is repeatedly invoked by the user application.

It should be noted that our framework doesn't introduce any additional hardware resource utilization compared to a hand-crafted OpenCL implementation.