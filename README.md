## SDx Guide for babbi

### Generic Makefile:
[https://github.com/lorenzoditucci/sda_examples_aws/blob/master/Makefile](https://github.com/lorenzoditucci/sda_examples_aws/blob/master/Makefile)

* **How to use:** make [emulation | build | clean | clean_sw_emu | clean_hw_emu | clean_hw | cleanall] TARGET=<sw_emu | hw_emu | hw>
* **KERNEL_LDCLFLAGS:** used to configure the memory bundles. They should also be set in the kernel C++ code. 
* **Things to specify:** KERNEL_SRC, KERNEL_EXE, KERNEL_NAME, TARGET_DEVICE (xilinx:adm-pcie-ku3:2ddr-xpr:4.0).

****

## How to run the "mmul" example:

### 1. Setup SDx
* On Raffaello:  
	`source /ku3_dsa/xbinst/setup.sh`  
	* To recompile the host:  
	`export XILINX_SDX=/opt/Xilinx/SDx/2017.2/`  
	`make host TARGET=build`
	
* On NAGS30:  
	`source /xilinx/software/SDx/2017.2/settings64.sh`  
* On Gozzo:  
	`source ~/coursera_dsa/xbinst/setup.sh`  
	* To recompile the host:  
	`export XILINX_SDX=/home/user/SDx/2017.2`  
	`make host TARGET=build`
	
* Check with `sdxsyschk` the platform name.
* Or use `sdxsyschk | grep <platform_short_name (e.g. 7v3)>`
	
### 2. Build for Hardware
* New -> Project... -> Xilinx -> Xilinx SDx Project...
* Set a project name.  
* Select a board:
	* Raffaello: adm-pcie-ku3 (Version 4.0)
	* Gozzo: Add Custom Platform -> 2017.1 -> platforms -> xilinx_adm-pcie-7v3_1ddr_3.0
* Next -> Next -> Empty Application -> Finish

* (Copy code into src if necessary).

* On the left, open project.sdx 
* Active build configuration: System (to build for the FPGA).
* Click on the "Fulmine", add the function to be accelerated.
* Rename with our default name "kernel_7v3" or "kernel_ku3"
* Press "Run" and pray.
	
	
### 3. Run on Hardware

* Clone the repository.
* Put the compiled host (.exe) and the compiled kernel (xclbin) in the same folder.
	* If the compiled host has not executable permission: `chmod u=rwx NAME_EXECUTABLE`
* Run the host and pray.

****

## How to run "var" with FIFOs:

### 1. Setup:
* Connect to Raffaello with 2 different shells (e.g. using ssh 2 times).
* `source /ku3_dsa/xbinst/setup.sh` in both terminals.

### 2. Run:
* In one shell, run `./var_fifo_sender`.
* In the other shell, run `./var_fifo_receiver`.

### Bonus: how to compile:
1. `./var_fifo_sender`: 
	g++ test-cl-fifo-sender.cpp -o var_fifo_sender
2. `./var_fifo_receiver`:
	* Connect and setup NAGS30.
	* In SDx, build for CPU-Emulation the project, and save `var.exe` as `./var_fifo_receiver`.

****

### Dependencies:
1. Astyle: [https://centos.pkgs.org/6/puias-computational-i386/astyle-2.05.1-3.sdl6.i686.rpm.html](https://centos.pkgs.org/6/puias-computational-i386/astyle-2.05.1-3.sdl6.i686.rpm.html)

****

### MATLAB solve mex gcc problem and launch:  
`source /opt/rh/devtoolset-2/enable` # Enable gcc 4.8 in centos  
`gcc --version`  # Ensure 4.8 enabled  
`/home/user/MATLAB/R2017a/bin/matlab` # From MATLAB open profax_fpga.m and run it  

****

### Installing Boost-Python (required for the Python interface):  
* Anaconda with Python3.6 is assumed to be installed in `/home/user`.  
* Original guide: [http://www.boost.org/doc/libs/1_66_0/more/getting_started/unix-variants.html](http://www.boost.org/doc/libs/1_66_0/more/getting_started/unix-variants.html).    
1. Download [http://www.boost.org/users/history/version_1_66_0.html](http://www.boost.org/users/history/version_1_66_0.html) into `/usr/local/`.  
2. `cd /usr/local/`  
3. `sudo tar --bzip2 -xf boost_1_66_0.tar.bz2`  
4. `sudo ./bootstrap.sh --with-python=/home/user/anaconda3/bin/python3.6 --with-libraries=python`  
5. `sudo ./b2 install`  

****

## OpenCL Guide:

* [https://www.fixstars.com/en/opencl/book/OpenCLProgrammingBook/opencl/](https://www.fixstars.com/en/opencl/book/OpenCLProgrammingBook/opencl/)