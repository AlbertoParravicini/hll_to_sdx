function y = acf_fpga(signal, lag_max, do_covariance)
    mex mkfifo/MATLAB_interface.cpp
    y = MATLAB_interface(signal, lag_max, do_covariance);
end