#include "../../host_mkfifo/host.hpp"
#include "mex.h"


/* Compute variance of the signal. */
float get_variance(float *x, int num_points) {
    float variance = 0.0;
    int i = 0;
    for (i = 0; i < num_points; i++) {
        variance += x[i] * x[i];
    }

    variance /= num_points;

    return variance;
}

/* The computational routine */
void acf_fpga_mex(double *signal, int num_points, int lag_max, int do_covariance, double *results) {

    int i = 0;

    float *x = (float*) calloc(num_points, sizeof(float));
    for (i = 0; i < num_points; i++) {
        x[i] = (float) signal[i];
    }

    /* Used to store the acf computed by the FPGA; */
    float *buff_out = (float*) calloc((lag_max + 1), sizeof(float));

    float variance = 1.0;
    if(!do_covariance)
        variance = get_variance(x, num_points);
    
    /* call host function */
    acf_host(x, num_points, lag_max, variance, buff_out);

    /* Copy result */   
    for (i = 0; i < lag_max + 1; i++) {
        results[i] = (double) buff_out[i];
    }
}


/* The gateway function */
void mexFunction(
                    int nlhs,               /* Number of output (left-side) arguments, or the size of the plhs array. */
                    mxArray *plhs[],        /* Array of output arguments. */
                    int nrhs,               /* Number of input (right-side) arguments, or the size of the prhs array. */
                    const mxArray *prhs[]   /* Array of input arguments. */
                )
{
    double *signal;                  /* 1xN input matrix: the signal */
    int lag_max;                     /* input scalar */
    int do_covariance;                     /* input scalar */
    double *outMatrix;               /* output matrix */
    
    int num_points;

    /* check for proper number of arguments */
    if(nrhs!=3) {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:nrhs","Three inputs required.");
    }
    if(nlhs!=1) {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:nlhs","One output required.");
    }
    
    /* make sure do_covariance and lag_max are scalar */
    if( !mxIsScalar(prhs[1]) || 
         mxIsComplex(prhs[1]) ||
         mxGetNumberOfElements(prhs[1])!=1 ) {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notScalar","lag_max must be a scalar.");
    }
    if( !mxIsScalar(prhs[2]) || 
         mxIsComplex(prhs[2]) ||
         mxGetNumberOfElements(prhs[2])!=1 ) {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notScalar","do_covariance must be a scalar.");
    }
    
    /* check on signal*/
    if (mxIsComplex(prhs[0])) {
        mexErrMsgTxt("Inputs must be real arrays.");
    }
    if (mxIsSparse(prhs[0])) {
        mexErrMsgTxt("Inputs must be dense arrays.");
    }
    if(mxGetM(prhs[0])!=1) {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notRowVector","Input must be a row vector.");
    }
    
    num_points = mxGetNumberOfElements(prhs[0]);
    
    /* create a pointer to the real data in the input matrix  */
    signal = mxGetPr(prhs[0]);
    
    /* get the value of the scalar inputS  */
    lag_max = mxGetScalar(prhs[1]);
    do_covariance = mxGetScalar(prhs[2]);

    /* create the output matrix */
    int output_size = lag_max + 1;
    plhs[0] = mxCreateNumericMatrix(1,output_size,mxDOUBLE_CLASS,mxREAL);

    /* get a pointer to the real data in the output matrix */
    outMatrix = mxGetPr(plhs[0]);

    /* call the computational routine */
    acf_fpga_mex(signal, num_points, lag_max, do_covariance, outMatrix);
}
