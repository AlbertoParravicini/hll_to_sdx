#ifndef MKFIFO_HOST_H_
#define MKFIFO_HOST_H_

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <riffa.h>;

#define LAGS_PER_ITERATION 208

void acf_host(float *signal, int num_points, int lag_max, float signal_variance, float *results) {

    int padded_lag_max = (lag_max + LAGS_PER_ITERATION - (lag_max + LAGS_PER_ITERATION) % LAGS_PER_ITERATION);
    float *buff_out = (float*) calloc(padded_lag_max, sizeof(float));

    // Parameters are casted to float to be passed
    float f_num_points = (float) num_points;
    float f_lag_max = (float) lag_max;
    
    // ===== OPEN CONNECTION =====
    // TODO
    fpga_t * fpga;
    int fid = 0; // FPGA id
    int channel = 0; // FPGA channel
    fpga = fpga_open(fid);
    
    // ===== SEND DATA =====
    fpga_send(fpga, channel, (void *)buf, BUF_SIZE, 0, 1, 0);
    
    // ===== RECEIVE DATA =====
    fpga_recv(fpga, channel, (void *)buf, BUF_SIZE, 0);
    
    // ===== CLOSE CONNECTION =====
    fpga_close(fpga);


    int i = 0;
    for (i = 0; i < lag_max + 1; i++) {
        results[i] = buff_out[i];
    }
}

#endif