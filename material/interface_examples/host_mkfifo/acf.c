#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>

#define LAGS_PER_ITERATION 208

void acf_cpu( // ============= PARAMETERS =============
        float *signal,                  // Vector representing the signal.
        int numPoints,                  // Number of points of the signal.
        int lagMax,                     // Lag max.
        float signal_variance,   		// 1 if the current core computes the covariance function insted of acf
        float *results                  // Pointer to vector in which save the results.
        ) {

    int lag = 0, pos = 0;

    // First compute covariance of the signal
    for (lag = 0; lag <= lagMax; lag++) {
        float sum = 0.0;

        for (pos = 0; pos < numPoints - lag; pos++)
            sum += signal[pos + lag] * signal[pos];

        results[lag] = sum / numPoints;
    }

    // Then compute autocorrelation of the signal
    for (lag = 0; lag <= lagMax; lag++) {
        results[lag] /= signal_variance;
    }
}

int main()
{
    float *x_in;
    float *out;

    // LINK THE THREE FIFO IN TMP FOLDER
    char *myfifo_x_in = "/tmp/myfifo_x_in";
    char *myfifo_out = "/tmp/myfifo_out";

    mkfifo(myfifo_x_in, 0666);
    mkfifo(myfifo_out, 0666);

    // OPEN THE THREE FIFO IN LETTURA E SCRITTURA
    int fifo_x_in;
    int fifo_out;
    fifo_x_in = open(myfifo_x_in, O_RDWR);
    fifo_out = open(myfifo_out, O_RDWR);

    printf("\nStreams opened.\n");


    int i = 0;
    int max_repetitions = 0;
    float f_num_points = 0;
    float f_lag_max = 0;
    float variance = 0.0;

    while(max_repetitions < 3){


        // Read data
        printf("ready to receive data...\n");

        printf("ready to read parameters...\n");
        read(fifo_x_in, &f_num_points, sizeof(float));
        read(fifo_x_in, &f_lag_max, sizeof(float));
        read(fifo_x_in, &variance, sizeof(float));

        int num_points = (int) f_num_points;
        int lag_max = (int) f_lag_max;

        printf("num_points: %i\n", num_points);
        printf("lag_max: %i\n", lag_max);
        printf("variance: %f\n", variance);

        int padded_lag_max = (lag_max + LAGS_PER_ITERATION - (lag_max + LAGS_PER_ITERATION) % LAGS_PER_ITERATION);

        x_in = (float*) calloc(num_points, sizeof(float));

        // Receive signal
        printf("ready to read signals...\n");
        read(fifo_x_in, &x_in[0], num_points*sizeof(float));

        out = (float*) calloc(padded_lag_max, sizeof(float));

        // acf
        acf_cpu(x_in, num_points, lag_max, variance, out);

        // Send data
        printf("start to send data...\n");

        write(fifo_out, out, padded_lag_max * sizeof(float));
        printf("data sent.\n\n");
        max_repetitions++;
    }

    // CHIUDI LE FIFO
    close(fifo_x_in);
    close(fifo_out);

    // UNLINK THE FIFO
    unlink(myfifo_x_in);
    unlink(myfifo_out);

    printf("END FIFO CLOSED\n");

    return 0;
}
