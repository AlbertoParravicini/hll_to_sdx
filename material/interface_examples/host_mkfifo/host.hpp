#ifndef MKFIFO_HOST_H_
#define MKFIFO_HOST_H_

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>

#define LAGS_PER_ITERATION 208

void acf_host(float *signal, int num_points, int lag_max, float signal_variance, float *results) {

    int padded_lag_max = (lag_max + LAGS_PER_ITERATION - (lag_max + LAGS_PER_ITERATION) % LAGS_PER_ITERATION);
    float *buff_out = (float*) calloc(padded_lag_max, sizeof(float));

    // Parameters are casted to float to be passed
    float f_num_points = (float) num_points;
    float f_lag_max = (float) lag_max;
    
    
    // ===== OPEN CONNECTION =====
    int fifo_x_in = open("/tmp/myfifo_x_in", O_WRONLY);
    int fifo_out = open("/tmp/myfifo_out", O_RDONLY);
    
    // ===== SEND DATA =====
    ssize_t res_trans;
    // Push each parameter at the beginning of the streams and wait until they are received by the core.
    res_trans = write(fifo_x_in, &f_num_points, sizeof(float));
    res_trans = write(fifo_x_in, &f_lag_max, sizeof(float));
    res_trans = write(fifo_x_in, &signal_variance, sizeof(float));

    // Send signal
    res_trans = write(fifo_x_in, &signal[0], num_points * sizeof(float));

    // ===== RECEIVE DATA =====
    res_trans = read(fifo_out, buff_out, padded_lag_max * sizeof(float));

    // ===== CLOSE CONNECTION =====
    close(fifo_x_in);
    close(fifo_out);


    int i = 0;
    for (i = 0; i < lag_max + 1; i++) {
        results[i] = buff_out[i];
    }
}

#endif