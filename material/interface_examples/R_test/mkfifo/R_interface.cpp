#include <stdlib.h>
#include <Rcpp.h>
#include "../../host_mkfifo/host.hpp"

using namespace Rcpp;

// Compute variance of the signal.
float get_variance(float *x, int num_points) {
    float variance = 0.0;
    int i = 0;
    for (i = 0; i < num_points; i++) {
        variance += x[i] * x[i];
    }

    variance /= num_points;

    return variance;
}


// [[Rcpp::export]]
NumericVector acf_fpga_rcpp(NumericVector signal, int lag_max, int do_covariance) {
  
    int i = 0;
    int num_points = signal.size();

    float *x = (float*) calloc(num_points, sizeof(float));
    for (i = 0; i < num_points; i++) {
        x[i] = signal[i];
    }

    // Used to store the acf computed by the FPGA;
    float *buff_out = (float*) calloc((lag_max + 1), sizeof(float));

    float variance = 1.0;
    if(!do_covariance)
        variance = get_variance(x, num_points);
    
    // call host function
    acf_host(x, num_points, lag_max, variance, buff_out);

    // Copy result into a NumericVector
    NumericVector out(lag_max + 1);
    
    for (i = 0; i < lag_max + 1; i++) {
        out[i] = buff_out[i];
    }
    
    return out;
}


/*** R
print("acf_fpga_rcpp imported correctly!")
*/