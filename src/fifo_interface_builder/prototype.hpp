#ifndef MKFIFO_HOST_H_
#define MKFIFO_HOST_H_

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>
#include <iostream>
#include <ctime>
#include <fstream>

void fifo_interface(__INSERT_FUNCTION_ARGS__)
{
    // Opening connections;
    int fifo_in = open("/tmp/myfifo_in", O_WRONLY);
    int fifo_out = open("/tmp/myfifo_out", O_RDONLY);

    // Used to measure execution time;
    timespec ts_start;
    timespec ts_end;
    
    //Sending data to the FIFOs;
    ssize_t res_trans;
    
    // Write the inputs on the FIFO;
    
    clock_gettime(CLOCK_REALTIME, &ts_start);
	__INSERT_INPUT_WRITE__
	clock_gettime(CLOCK_REALTIME, &ts_end);
     double exec_time = (double)(ts_end.tv_nsec - ts_start.tv_nsec) / 1000000000;
     std::cout << "--> FPGA FIFO INPUT TIME=" << exec_time << std::endl;

	 // Store the execution time;
     std::ofstream output_file;
     output_file.open("../../../data/host.csv", std::ios_base::app);
     output_file << "__INSERT_KERNEL_NAME__, , fifo_input , " << exec_time  << std::endl;
     output_file.close();

    // Read the outputs from the FIFOs;
    __INSERT_OUTPUT_WRITE__
    
    // Close the FIFOs;
    close(fifo_in);
    close(fifo_out);
}

#endif
