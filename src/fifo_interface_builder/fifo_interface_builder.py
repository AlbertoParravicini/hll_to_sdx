import json
import subprocess
import argparse
import numpy as np


def build_fifo_interface(config_dict, interface_prototype):
    
    fifo_input_a = "res_trans = write(fifo_in, {}, sizeof({}) * {});\n"
    fifo_input_s = "res_trans = write(fifo_in, &{}, sizeof({}));\n"
    fifo_output_a = "res_trans = read(fifo_out, {}, sizeof({}) * {});\n"
    fifo_output_s = "res_trans = read(fifo_out, {}, sizeof({}));\n"
    
    # Input/output list;
    io_list = config_dict["inputs"] + config_dict["outputs"]
    # Sort the previous list by the argument position;
    io_list = [io_list[j] for j in np.argsort([i["position"] for i in io_list])]
    
    # Build inputs and outputs arguments;
    arguments_declaration = ""
    for i in io_list:
        if i in config_dict["inputs"] and i["type"] == "array":
            arguments_declaration += i["class"] + " *" + i["name"] + ", "
        elif i in config_dict["inputs"] and i["type"] == "scalar":
            arguments_declaration += i["class"] + " " + i["name"] + ", "
        elif i in config_dict["outputs"]:
            arguments_declaration += i["class"] + " *" + i["name"] + ", "
    # Remove the last ",", then add the arguments to the interface file;
    interface_src = interface_prototype.replace("__INSERT_FUNCTION_ARGS__", arguments_declaration[:-2])         
            
    # Sort the input list;
    input_list = [config_dict["inputs"][j] for j in np.argsort([i["position"] for i in config_dict["inputs"]])]
    input_fifo_declaration = ""
    for i in input_list:
        if i["type"] == "array":
            input_fifo_declaration += fifo_input_a.format(i["name"], i["class"], i["length"])
        elif i["type"] == "scalar":
            input_fifo_declaration += fifo_input_s.format(i["name"], i["class"])
    interface_src = interface_src.replace("__INSERT_INPUT_WRITE__\n", input_fifo_declaration)         
            
    # Sort the output list;
    output_list = [config_dict["outputs"][j] for j in np.argsort([i["position"] for i in config_dict["outputs"]])]
    output_fifo_declaration = ""
    for o in output_list:
        if o["type"] == "array":
            output_fifo_declaration += fifo_output_a.format(o["name"], o["class"], o["length"])
        elif o["type"] == "scalar":
            output_fifo_declaration += fifo_output_s.format(o["name"], o["class"])    
    interface_src = interface_src.replace("__INSERT_OUTPUT_WRITE__\n", output_fifo_declaration)   

	# Add the function name;
    interface_src = interface_src.replace("__INSERT_FUNCTION_NAME__", config_dict["kernel_name"])   
        
    return interface_src


if __name__ == "__main__":
    
    # Used to parse the input arguments;
    parser = argparse.ArgumentParser(description="Build an interface that communicates with the OpenCL host through FIFOs;\
                                     the interface is unified between all the supported languages,\
                                     and is described by a configuration JSON")
    parser.add_argument("-i", "--config", metavar="<path/to/kernel/config>",\
                        help="Path to the a JSON file that describes the kernel for which the interface is built.")
    parser.add_argument("-p", "--prototype", metavar="<path/to/the/interface/prototype>",\
                        help="Path to the C++ file used as prototype to build the interface.")
    parser.add_argument("-o", "--output", metavar="<path/to/the/interface/file>",\
                        help="Path to where the interface file will be written.")   
    # Parse the input arguments;
#    args = parser.parse_args(["-i", "../../examples/mmult/src/mmult_config.json",
#                              "-p", "prototype.hpp",
#                              "-o", "interface.hpp",
#                              ])
    args = parser.parse_args()
    
    config_path = args.config
    prototype_path = args.prototype
    interface_path = args.output
    
    
    # Read the JSON;
    with open(config_path, "r") as f:
        config_dict = json.loads(f.read())
        
    # Read the prototype host;
    with open(prototype_path, "r") as f:
        interface_prototype = f.read()
        
    # Build the host source code;  
    interface_src = build_fifo_interface(config_dict, interface_prototype)
    
    # Save the source code;
    with open(interface_path, "w+") as f:
        f.write(interface_src)
    
    # Format the code;
    subprocess.run(["astyle", "--style=allman", "--suffix=none", interface_path])
    
    