import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import statsmodels.tsa.stattools as stat
from matplotlib.ticker import AutoMinorLocator
import matplotlib.gridspec as gridspec
import seaborn as sns

g1 = "#3D3D3D"
g2 = "#686868"
g3 = "#919191"
g4 = "#C1BDBD"
g5 = "#EFEDED"

plt.rc("axes.spines", top=False, right=False)

if __name__ == "__main__":
    data_host = pd.read_csv("../data/new_host.csv", names=["kernel", "lang", "step", "time"], sep=", ")
    data_host = data_host.drop(columns=["lang"])
    data_mmult = pd.read_csv("../data/new_mmult_overall.csv", names=["kernel", "lang", "step", "time"], sep=", ")
    data_var = pd.read_csv("../data/new_var_overall.csv", names=["kernel", "lang", "step", "time"], sep=", ")
    
    data_mmult["time"] = data_mmult["time"] * 1000
    data_var["time"] = data_var["time"] * 1000
    data_host["time"] = data_host["time"] * 1000
    
    #%% Plot the execution times of HW and SW for mmult and var.
    data_overall = pd.concat([data_mmult, data_var])
    
    data_overall["lang"] = data_overall["lang"].replace(["python", "r", "matlab"], ["Python", "R", "MATLAB"])
    
    # Group the values by language and step;
    sns.set(font_scale=3)
    sns.set_style("whitegrid")
    
    gs = gridspec.GridSpec(2, 1)
    fig, axes = plt.subplots(nrows=2, sharex=True, figsize=(8, 6))
    
    mmult_ax = sns.barplot(data=data_overall[data_overall["kernel"] == "mmult"],
                       hue_order=["overall_sw", "overall_hw"],
                       x="lang", y="time", hue="step", 
                       estimator=np.median, saturation=0.8, alpha=0.8,
                       palette=[g1, g3], ax=plt.subplot(gs[0]))
    
    var_ax = sns.barplot(data=data_overall[data_overall["kernel"] == "var"],
                       hue_order=["overall_sw", "overall_hw"],
                       x="lang", y="time", hue="step", 
                       estimator=np.median, saturation=0.8, alpha=0.8,
                       palette=[g1, g3], ax=plt.subplot(gs[1]))

    mmult_ax.set_yscale("log", nonposy='clip')
    
    mmult_ax.set_xlabel("Language", fontsize=20)
    mmult_ax.set_ylabel("Execution time [sec]", fontsize=20)
    
    mmult_ax.legend().set_visible(False)
    
    # Define some hatches
    hatches = [""] * 3 +  ['\\'] * 3
    # Loop over the bars
    for i, thisbar in enumerate(mmult_ax.patches):
        # Set a different hatch for each bar
        thisbar.set_hatch(hatches[i])

    
    var_ax.set_yscale("log", nonposy='clip')
    
    var_ax.set_xlabel("Language", fontsize=20)
    var_ax.set_ylabel("Execution time [sec]", fontsize=20)
    
    plt.legend(loc=1)
    
    # Define some hatches
    hatches = [""] * 3 +  ['\\'] * 3
    # Loop over the bars
    for i, thisbar in enumerate(var_ax.patches):
        # Set a different hatch for each bar
        thisbar.set_hatch(hatches[i])
        
#    plt.tight_layout()
    
    
    #%% Plot the division between kernel and overhead.
    
    # Group the host.
    host = data_host.groupby(["kernel", "step"], as_index=False).median()
    host["time_var"] = list(data_host.groupby(["kernel", "step"]).var()["time"])
    
    data_mmult_hw = data_mmult[data_mmult.step == "overall_hw"]
    
    data_mmult_hw["kernel_time"] = host[(host.kernel == "mmult") & (host.step == "board_exec")].time.iat[0]
    data_mmult_hw["overhead"] = data_mmult_hw["time"] - data_mmult_hw["kernel_time"]
    
    data_var_hw = data_var[data_var.step == "overall_hw"]
    
    data_var_hw["kernel_time"] = host[(host.kernel == "var") & (host.step == "board_exec")].time.iat[0]
    data_var_hw["overhead"] = data_var_hw["time"] - data_var_hw["kernel_time"]
    
    data_overhead = pd.concat([data_mmult_hw, data_var_hw])
    
    data_overhead["lang"] = data_overhead["lang"].replace(["python", "r", "matlab"], ["Python", "R", "MATLAB"])

    
    #%% Plot the mmult values
    plt.figure()
    
    sns.set(font_scale=2)
    sns.set_style("whitegrid")
    plt.title('Mmult - Overheads')
    sns.barplot(x = "lang", y = "time", color = g1, data=data_overhead[data_overhead["kernel"] == "mmult"], alpha=0.8)   
    #Plot 2 - overlay - "bottom" series
    bottom_plot = sns.barplot(x = "lang", y = "overhead", color = g4, data=data_overhead[data_overhead["kernel"] == "mmult"], alpha=0.8)
    
    # Define some hatches
    hatches = ['\\'] * 3
    # Loop over the bars
    for i, thisbar in enumerate(bottom_plot.patches[3:]):
        # Set a different hatch for each bar
        thisbar.set_hatch(hatches[i])
    
    topbar = plt.Rectangle((0,0),1,1,fc=g1, edgecolor = 'none')
    bottombar = plt.Rectangle((0,0),1,1,fc=g4,  edgecolor = 'none')
    l = plt.legend([bottombar, topbar], ['Overhead', 'Kernel Execution Time'], loc=1, ncol = 1, prop={'size':20})
    l.draw_frame(False)
    
    bottom_plot.set_xlabel("Language", fontsize=20)
    bottom_plot.set_ylabel("Execution time [sec]", fontsize=20)
    
    sns.set(rc={'figure.figsize':(14,10)})
    plt.tight_layout()   
    
    plt.subplots_adjust(left=0.15)
    
    
    
    #%% Plot both the mmult and var values
    plt.figure()
    
    sns.set(font_scale=3)
    sns.set_style("whitegrid")
    plt.title('Interfaces Overheads')
    

    
    sns.barplot(x = "kernel", y = "time", palette=[g1, g1, g1], data=data_overhead, alpha=1, hue="lang")   
    #Plot 2 - overlay - "bottom" series
    bottom_plot = sns.barplot(x = "kernel", y = "overhead", palette=[g3, g4, g5], data=data_overhead, hue="lang", alpha=1)
    
    # Define some hatches
    hatches = ['\\'] * 6
    # Loop over the bars
    for i, thisbar in enumerate(bottom_plot.patches[6:]):
        # Set a different hatch for each bar
        thisbar.set_hatch(hatches[i])
    
    ovbar = plt.Rectangle((0,0),1,1,fc=g1, edgecolor = 'none')
    pybar = plt.Rectangle((0,0),1,1,fc=g3,  edgecolor = 'none')
    matbar = plt.Rectangle((0,0),1,1,fc=g4,  edgecolor = 'none')
    rbar = plt.Rectangle((0,0),1,1,fc=g5,  edgecolor = 'none')
    l = plt.legend([ovbar, pybar, matbar, rbar], ['Kernel Execution Time', 'Python Overhead', 'MATLAB Overhead' , 'R Overhead'], ncol = 2, prop={'size':25}, loc=2, borderaxespad=0.)
    l.draw_frame(False)
    
    bottom_plot.set_xlabel("Kernel")
    bottom_plot.set_ylabel("Execution time [ms]")
    
    bottom_plot.set_yscale("log", nonposy='clip')
    
    sns.set(rc={'figure.figsize':(14,10)})
    plt.tight_layout()   
    
    plt.subplots_adjust(left=0.15)
    
    
    #%% Plot an horizontal bar to show the steps;
    
    
    # Add the estimated overhead;
    grouped_mmult = data_mmult.groupby(["kernel", "lang", "step"], as_index=False).median()
    mmult_overhead = (grouped_mmult[(grouped_mmult["kernel"] == "mmult") & (grouped_mmult["step"] == "overall_hw") & (grouped_mmult["lang"] == "python")].time.iat[0] -\
                    (host[(host["kernel"] == "mmult") & (host["step"] == "fpga_input")].time.iat[0] +\
                     host[(host["kernel"] == "mmult") & (host["step"] == "board_exec")].time.iat[0] +\
                     host[(host["kernel"] == "mmult") & (host["step"] == "fifo_output")].time.iat[0])) / 3
  
    grouped_var = data_var.groupby(["kernel", "lang", "step"], as_index=False).median()          
    var_overhead = (grouped_var[(grouped_var["kernel"] == "var") & (grouped_var["step"] == "overall_hw") & (grouped_var["lang"] == "python")].time.iat[0] -
                    (host[(host["kernel"] == "var") & (host["step"] == "fpga_input")].time.iat[0] +
                     host[(host["kernel"] == "var") & (host["step"] == "board_exec")].time.iat[0] +
                     host[(host["kernel"] == "var") & (host["step"] == "fifo_output")].time.iat[0]))
    
    overhead = np.array([mmult_overhead, var_overhead])
    plt.figure()
    
    sns.set(font_scale=3)
    sns.set_style("whitegrid")
    plt.title('Time distribution')
    
    p1 = plt.barh(["mmult", "var"], [overhead[0]*2, overhead[1]], color=g4, height=0.5)
    p2 = plt.barh(["mmult", "var"], host[host["step"] == "fpga_input"].time, color=g2, left=[overhead[0]*2, overhead[1]], height=0.5)
    p3 = plt.barh(["mmult", "var"], host[host["step"] == "board_exec"].time, left=[overhead[0]*2, overhead[1]] + host[host["step"] == "fpga_input"].time, color=g1, height=0.5)
    p4 = plt.barh(["mmult", "var"],
                 host[host["step"] == "fifo_output"].time,
                 left=[overhead[0]*2, overhead[1]] + (np.array(host[host["step"] == "board_exec"].time) + np.array(host[host["step"] == "fpga_input"].time)),
                 color=g3, height=0.5)
    p5 = plt.barh(["mmult", "var"],
                 [overhead[0], 0],
                 left=[overhead[0]*2, overhead[1]] + (np.array(host[host["step"] == "board_exec"].time) + np.array(host[host["step"] == "fpga_input"].time)) + host[host["step"] == "fifo_output"].time,
                 color=g4, height=0.5)

    
    # Define some hatches
    hatches = ['\\'] * 2
    # Loop over the bars
    for i, thisbar in enumerate(p1.patches):
        # Set a different hatch for each bar
        thisbar.set_hatch(hatches[i])
    for i, thisbar in enumerate(p5.patches):
        # Set a different hatch for each bar
        thisbar.set_hatch(hatches[i])
        
    plt.legend((p1[0], p2[0], p3[0], p4[0]), ('Interface Overhead', 'FPGA Data Transfer', 'FPGA Execution', 'FIFO output'), loc=4)    
        
    plt.xlabel("Execution time [ms]")
    plt.ylabel("Kernel")
    
    plt.tight_layout()   
    
    
        
    
    
    
    
    
    
    
    