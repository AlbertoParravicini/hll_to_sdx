#!/usr/bin/env python

from distutils.core import setup
from distutils.extension import Extension

setup(name="PackageName",
    ext_modules=[
        Extension("python_interface", ["python_interface.cpp"],
        libraries = ["boost_python"])
    ])
