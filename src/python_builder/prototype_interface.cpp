#include <stdlib.h>
#include <boost/python/module.hpp>
#include <boost/python/def.hpp>
#include <boost/python.hpp> 
#include <iostream>
#include "../fifo_interface.hpp"

using namespace boost::python;


template <class T>
static list to_python_list(T *in_vec, int length)
{
	int i = 0;
	list out_vec;

	for (i=0; i<length; i++)
	{
	    out_vec.append(in_vec[i]);
	}
	return out_vec;
}


__INSERT_OUTPUT_TYPE__ __INSERT_FUNCTION_NAME___fpga(__INSERT_FUNCTION_ARGS__)
{

	// Index used to cast inputs, if required;
	int i=0;


    // Cast the inputs, if required;
    __INSERT_INPUT_CAST__


    // Allocate memory to store the output;
    __INSERT_OUTPUT_ALLOC__


	fifo_interface(__INSERT_FIFO_PARAMS__);


	// Free the memory;
    __INSERT_FREE_MEM__

    
    return __INSERT_OUTPUT_NAME__;
}


BOOST_PYTHON_MODULE(python_interface)
{
    def("__INSERT_FUNCTION_NAME___fpga", __INSERT_FUNCTION_NAME___fpga);
}
