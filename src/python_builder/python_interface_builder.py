import json
import argparse
import numpy as np
import subprocess


def find_python_type(i):
    if i["type"] == "array":
        return "list"
    elif i["type"] == "scalar":
        return i["class"]
        

def build_python_interface(config_dict, python_interface_prototype):
    input_cast_a = "{} *{}_cast = ({} *) malloc({} * sizeof({}));\nfor(i=0; i<{}; i++)\n{{\n{}_cast[i] = extract<{}>({}[i]);\n}}\n"
    input_cast_s = "{} {}_cast = ({}) {};\n"
    output_alloc_a = "{} *{}_cast = ({} *) malloc({} * sizeof({}));\n"
    output_alloc_s = "{} *{}_cast = ({} *) malloc(sizeof({}));\n"
    
    
    # Sort the input list;
    input_list = [config_dict["inputs"][j] for j in np.argsort([i["position"] for i in config_dict["inputs"]])]
    # Sort the output list;
    output_list = [config_dict["outputs"][j] for j in np.argsort([i["position"] for i in config_dict["outputs"]])]
    
    # Insert the function name;
    python_interface_src = python_interface_prototype.replace("__INSERT_FUNCTION_NAME__", config_dict["kernel_name"])
    
    # Insert the inputs;
    inputs_declaration = ""
    for i in input_list:
        if i["type"] == "array":
            inputs_declaration += "const list " + i["name"] + ", "
        elif i["type"] == "scalar":
            inputs_declaration += "const " + i["class"] + " " + i["name"] + ", "
    python_interface_src = python_interface_src.replace("__INSERT_FUNCTION_ARGS__", inputs_declaration[:-2])
    
    # Insert the output type;
    output_type = ""
    if len(output_list) > 1:
        output_type = "list"
    else:
        output_type = find_python_type(output_list[0])
    python_interface_src = python_interface_src.replace("__INSERT_OUTPUT_TYPE__", output_type)
    
    
    # Cast the inputs to the appropriate type;
    input_cast_declaration = ""
    for i in input_list:
        if i["type"] == "array":
            input_cast_declaration += input_cast_a.format(i["class"], i["name"], i["class"], i["length"], i["class"], i["length"], i["name"], i["class"], i["name"])
        elif i["type"] == "scalar":
            input_cast_declaration += input_cast_s.format(i["class"], i["name"], i["class"], i["name"]);
    python_interface_src = python_interface_src.replace("__INSERT_INPUT_CAST__\n", input_cast_declaration)         
            
    # Initialize the outputs to the appropriate types;
    output_alloc_declaration = ""
    for o in output_list:
        if o["type"] == "array":
            output_alloc_declaration += output_alloc_a.format(o["class"], o["name"], o["class"], o["length"], o["class"]);
        elif o["type"] == "scalar":
            output_alloc_declaration += output_alloc_s.format(o["class"], o["name"], o["class"], o["class"]);
    python_interface_src = python_interface_src.replace("__INSERT_OUTPUT_ALLOC__\n", output_alloc_declaration)      
        
    # Call the FPGA;
    fifo_call = ""
    for i in input_list + output_list:
        fifo_call += i["name"] + "_cast, "
    python_interface_src = python_interface_src.replace("__INSERT_FIFO_PARAMS__", fifo_call[:-2])  

    # Free the memory;
    free_mem = ""
    for i in input_list:
        if i["type"] == "array":
            free_mem += "free({}_cast);\n".format(i["name"])
    python_interface_src = python_interface_src.replace("__INSERT_FREE_MEM__\n", free_mem)
    
    # Insert the output name;
    output_name = ""
    for o in output_list:
        if o["type"] == "array":
            output_name += "to_python_list(" + o["name"] + "_cast, " + str(o["length"]) + "), "
        elif o["type"] == "scalar":
            output_name += "*" + o["name"] + "_cast, "
    if len(output_list) > 1:
        output_name = "to_python_list([" + output_name[:-2] + "], "+ str(len(output_list)) + ")"
    else:
        output_name = output_name[:-2]
    python_interface_src = python_interface_src.replace("__INSERT_OUTPUT_NAME__", output_name)      
    
    return python_interface_src


if __name__ == "__main__":
    
    # Used to parse the input arguments;
    parser = argparse.ArgumentParser(description="Build the python interface that calls the FPGA.")
    parser.add_argument("-i", "--config", metavar="<path/to/kernel/config>",
                        help="Path to the a JSON file that describes the kernel for which the interface is built.")
    parser.add_argument("-p", "--prototype", metavar="<path/to/the/python/interface/prototype>",
                        help="Path to the C++ file used as prototype to build the python interface.")
    parser.add_argument("-o", "--output", metavar="<path/to/the/python/interface/file>",
                        help="Path to where the python interface file will be written.")   
    # Parse the input arguments;
#    args = parser.parse_args(["-i", "../../examples/var/src/config.json",
#                              "-p", "prototype_interface.cpp",
#                              "-o", "interface.cpp",
#                              ])
    args = parser.parse_args()
    
    config_path = args.config
    prototype_path = args.prototype
    python_interface_path = args.output
    
    
    # Read the JSON;
    with open(config_path, "r") as f:
        config_dict = json.loads(f.read())
        
    # Read the prototype host;
    with open(prototype_path, "r") as f:
        python_interface_prototype = f.read()
        
    # Build the python source code;  
    python_interface_src = build_python_interface(config_dict, python_interface_prototype)
    
    # Save the source code;
    with open(python_interface_path, "w+") as f:
        f.write(python_interface_src)
        
    # Format the code;
    subprocess.run(["astyle", "--style=allman", "--suffix=none", python_interface_path])
    

