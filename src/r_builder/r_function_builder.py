import json
import argparse
import numpy as np


def build_r_function(config_dict, r_function_prototype):
    
    # Sort the input list;
    input_list = [config_dict["inputs"][j] for j in np.argsort([i["position"] for i in config_dict["inputs"]])]
    input_declaration = ""
    for i in input_list:
        input_declaration += i["name"] + ", "
    r_function_src = r_function_prototype.replace("__INSERT_INPUTS__", input_declaration[:-2])         

    # Specify the function name;
    r_function_src = r_function_src.replace("__INSERT_FUNCTION_NAME__", config_dict["kernel_name"])
        
    return r_function_src


if __name__ == "__main__":
    
    # Used to parse the input arguments;
    parser = argparse.ArgumentParser(description="Build the r function that calls the FPGA.")
    parser.add_argument("-i", "--config", metavar="<path/to/kernel/config>",\
                        help="Path to the a JSON file that describes the kernel for which the interface is built.")
    parser.add_argument("-p", "--prototype", metavar="<path/to/the/r/function/prototype>",\
                        help="Path to the C++ file used as prototype to build the r function.")
    parser.add_argument("-o", "--output", metavar="<path/to/the/r/function/file>",\
                        help="Path to where the r function file will be written.")   
    # Parse the input arguments;
#    args = parser.parse_args(["-i", "../../examples/var/src/config.json",
#                              "-p", "prototype_function.r",
#                              "-o", "function.r",
#                              ])
    args = parser.parse_args()
    
    config_path = args.config
    prototype_path = args.prototype
    r_function_path = args.output
    
    
    # Read the JSON;
    with open(config_path, "r") as f:
        config_dict = json.loads(f.read())
        
    # Read the prototype host;
    with open(prototype_path, "r") as f:
        r_function_prototype = f.read()
        
    # Build the host source code;  
    r_function_src = build_r_function(config_dict, r_function_prototype)
    
    # Save the source code;
    with open(r_function_path, "w+") as f:
        f.write(r_function_src)
    
    