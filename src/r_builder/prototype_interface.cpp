#include <stdlib.h>
#include <Rcpp.h>
#include "../fifo_interface.hpp"

using namespace Rcpp;

// [[Rcpp::export]]
__INSERT_OUTPUT_TYPE__ r_fifo_interface(__INSERT_INPUTS__)
{
    
    // Index used to cast inputs, if required;
    int i = 0;

    
    // Cast the inputs, if required;
    __INSERT_INPUT_CAST__


    // Allocate memory to store the output;
    __INSERT_OUTPUT_ALLOC__

    
    // Call the FPGA;
    fifo_interface(__INSERT_FIFO_PARAMS__);

    
    // Write the output;
    __INSERT_OUTPUT_DEF__
    __INSERT_OUTPUT_CAST__


    // Free the memory;
    __INSERT_FREE_MEM__

    
    return __INSERT_OUTPUT_NAME__;
}


/*** R
print("r_sender_interface imported correctly!")
*/