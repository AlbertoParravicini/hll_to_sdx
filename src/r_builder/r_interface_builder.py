import json
import argparse
import numpy as np
import subprocess


def find_r_type(i):
    if i["type"] == "scalar":
        if i["class"] == "int":
            return "int"
        elif i["class"] == "float":
            return "double"
    elif i["type"] == "array":
        if i["class"] == "int":
            return "IntegerVector"
        elif i["class"] == "float":
            return "NumericVector"
        

def build_r_interface(config_dict, r_interface_prototype):
    input_cast_a = "{} *{}_cast = ({} *) malloc({} * sizeof({}));\nfor(i=0; i<{}; i++)\n{{\n{}_cast[i] = ({}) {}[i];\n}}\n"
    input_cast_s = "{} {}_cast = ({}) {};\n"
    output_alloc_a = "{} *{}_cast = ({} *) malloc({} * sizeof({}));\n"
    output_alloc_s = "{} *{}_cast = ({} *) malloc(sizeof({}));\n"
    output_cast_a = "for(i=0; i<{}; i++)\n{{\n{}[i] = ({}) {}_cast[i];\n}}\n"
    output_cast_s = "*{} = ({}) *{}_cast;\n "
    
    
    # Sort the input list;
    input_list = [config_dict["inputs"][j] for j in np.argsort([i["position"] for i in config_dict["inputs"]])]
    # Sort the output list;
    output_list = [config_dict["outputs"][j] for j in np.argsort([i["position"] for i in config_dict["outputs"]])]
    
    if len(output_list) > 1:
        raise ValueError("R supports at most 1 output")
        
    # Add the return type;
    r_interface_src = r_interface_prototype.replace("__INSERT_OUTPUT_TYPE__", find_r_type(output_list[0]))      
    
    # Add the caster arguments;
    caster_args = ""
    for i in input_list:
        caster_args += find_r_type(i) + " " + i["name"] + ", "
    r_interface_src = r_interface_src.replace("__INSERT_INPUTS__", caster_args[:-2])      
    
    # Cast the inputs to the appropriate type;
    input_cast_declaration = ""
    for i in input_list:
        if i["type"] == "array":
            input_cast_declaration += input_cast_a.format(i["class"], i["name"], i["class"], i["length"], i["class"], i["length"], i["name"], i["class"], i["name"])
        elif i["type"] == "scalar":
            input_cast_declaration += input_cast_s.format(i["class"], i["name"], i["class"], i["name"]);
    r_interface_src = r_interface_src.replace("__INSERT_INPUT_CAST__\n", input_cast_declaration)         
            
    # Initialize the outputs to the appropriate types;
    output_alloc_declaration = ""
    for o in output_list:
        if o["type"] == "array":
            output_alloc_declaration += output_alloc_a.format(o["class"], o["name"], o["class"], o["length"], o["class"]);
        elif o["type"] == "scalar":
            output_alloc_declaration += output_alloc_s.format(o["class"], o["name"], o["class"], o["class"]);
    r_interface_src = r_interface_src.replace("__INSERT_OUTPUT_ALLOC__\n", output_alloc_declaration)      
        
    # Call the FPGA;
    fifo_call = ""
    for i in input_list + output_list:
        fifo_call += i["name"] + "_cast, "
    r_interface_src = r_interface_src.replace("__INSERT_FIFO_PARAMS__", fifo_call[:-2])  
    
    # Allocate the output;
	output_cast = ""
    o = output_list[0]
    cast_type = ""
    if o["class"] == "int":
        cast_type = "int"
    else:
        cast_type = "double"
    if output_list[0]["type"] == "array":
        output_allocation = find_r_type(output_list[0]) + output_list[0]["name"]
    else:
        output_allocation = find_r_type(output_list[0]) + output_list[0]["name"] + " = " + output_cast_s.format(o["name"], cast_type, o["name"])
    if output_list[0]["type"] == "array":
        output_allocation += "({})".format(output_list[0]["length"]) + ";\n"
    else:
        output_allocation += ";\n"
    r_interface_src = r_interface_src.replace("__INSERT_OUTPUT_DEF__\n", output_allocation)       
    
    # Store the outputs;       
    if o["type"] == "array":
        output_cast += output_cast_a.format(o["length"], o["name"], cast_type, o["name"])
    r_interface_src = r_interface_src.replace("__INSERT_OUTPUT_CAST__\n", output_cast)

    # Free the memory;
    free_mem = ""
    for i in input_list + output_list:
        if i["type"] == "array":
            free_mem += "free({}_cast);\n".format(i["name"])
    r_interface_src = r_interface_src.replace("__INSERT_FREE_MEM__\n", free_mem)
    
    # Insert the output name;
    if output_list[0]["type"] == "array":
        r_interface_src = r_interface_src.replace("__INSERT_OUTPUT_NAME__", output_list[0]["name"])
    else:
        r_interface_src = r_interface_src.replace("__INSERT_OUTPUT_NAME__", output_list[0]["name"])
    
    
    return r_interface_src


if __name__ == "__main__":
    
    # Used to parse the input arguments;
    parser = argparse.ArgumentParser(description="Build the R interface that calls the FPGA.")
    parser.add_argument("-i", "--config", metavar="<path/to/kernel/config>",
                        help="Path to the a JSON file that describes the kernel for which the interface is built.")
    parser.add_argument("-p", "--prototype", metavar="<path/to/the/r/interface/prototype>",
                        help="Path to the C++ file used as prototype to build the R interface.")
    parser.add_argument("-o", "--output", metavar="<path/to/the/r/interface/file>",
                        help="Path to where the R interface file will be written.")   
    # Parse the input arguments;
#    args = parser.parse_args(["-i", "../../examples/var/src/config.json",
#                              "-p", "prototype_interface.cpp",
#                              "-o", "interface.cpp",
#                              ])
    args = parser.parse_args()
    
    config_path = args.config
    prototype_path = args.prototype
    r_interface_path = args.output
    
    
    # Read the JSON;
    with open(config_path, "r") as f:
        config_dict = json.loads(f.read())
        
    # Read the prototype host;
    with open(prototype_path, "r") as f:
        r_interface_prototype = f.read()
        
    # Build the host source code;  
    r_interface_src = build_r_interface(config_dict, r_interface_prototype)
    
    # Save the source code;
    with open(r_interface_path, "w+") as f:
        f.write(r_interface_src)
        
    # Format the code;
    subprocess.run(["astyle", "--style=allman", "--suffix=none", r_interface_path])
    

