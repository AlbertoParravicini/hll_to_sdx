#include "../fifo_interface.hpp"
#include "mex.h"
#include <sys/time.h>
#include <iostream>
#include <ctime>
#include <fstream>

/* TODO: Auto generate params */
void matlab_sender_interface(__INSERT_CASTER_ARGS__)
{

    /* Index used to cast arguments, if required;*/
    int i = 0;

    
    /* Cast the input arguments, if required;*/
    __INSERT_INPUT_CAST__

    
    /* Allocate the output arguments;*/
    __INSERT_OUTPUT_ALLOC__

 
    /* Call the FPGA;*/
    fifo_interface(__INSERT_FIFO_PARAMS__);

    
    /* Cast the output of the FPGA to doubles, to be used in MATLAB;*/
    __INSERT_OUTPUT_CAST__


    /* Free the memory*/
    __INSERT_FREE_MEM__
}


/* The gateway function */
void mexFunction(
                    int nlhs,               /* Number of output (left-side) arguments, or the size of the plhs array. */
                    mxArray *plhs[],        /* Array of output arguments. */
                    int nrhs,               /* Number of input (right-side) arguments, or the size of the prhs array. */
                    const mxArray *prhs[]   /* Array of input arguments. */
                )
{

    /* Check for the proper number of arguments */
    if(nrhs!=__INSERT_INPUT_NUM__) {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:nrhs","Wrong number of inputs.");
    }
    if(nlhs!=__INSERT_OUTPUT_NUM__) {
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:nlhs","Wrong number of outputs.");
    }
    

    /* Check the type of the parameters, we assume that they are double;
    Arrays are required to be row vectors;*/
    __INSERT_SCALAR_TYPE_CHECK__

    __INSERT_ARRAY_TYPE_CHECK__


    /* Get the value of each input;*/
    __INSERT_READ_SCALAR__

    __INSERT_READ_ARRAY__

   
    /* Create the output variables;*/
    __INSERT_ALLOC_OUTPUT_VAR__


    /* Get a pointer to the real data in the output matrix */
    __INSERT_GET_OUTPUT_PTR__


    /* Call the computational routine */
    matlab_sender_interface(__INSERT_CASTER_CALL__);
}
