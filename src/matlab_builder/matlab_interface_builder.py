import json
import argparse
import numpy as np
import subprocess


def build_matlab_interface(config_dict, matlab_interface_prototype):
    input_cast_a = "{} *{}_cast = ({} *) malloc({} * sizeof({}));\nfor(i=0; i<{}; i++)\n{{\n{}_cast[i] = ({}) {}[i];\n}}\n"
    input_cast_s = "{} {}_cast = ({}) *{};\n"
    output_alloc_a = "{} *{}_cast = ({} *) malloc({} * sizeof({}));\n"
    output_alloc_s = "{} *{}_cast = ({} *) malloc(sizeof({}));\n"
    output_cast_a = "for(i=0; i<{}; i++)\n{{\n{}[i] = (double) {}_cast[i];\n}}\n"
    output_cast_s = "*{} = (double) *{}_cast;\n "
    scalar_input_check_s = "if( !mxIsScalar(prhs[{}]) ||\nmxIsComplex(prhs[{}]) ||\nmxGetNumberOfElements(prhs[{}])!=1 )\
    {{\nmexErrMsgIdAndTxt(\"MyToolbox:arrayProduct:notScalar\",\"The input argument must be a scalar.\");\n}}\n"
    array_input_check_a = "if (mxIsComplex(prhs[{}])) {{\nmexErrMsgTxt(\"The input must be a real array.\");\n}}\n\
    if (mxIsSparse(prhs[{}])) {{\nmexErrMsgTxt(\"The input must be a dense array.\");\n}}\n\
    if(mxGetM(prhs[{}])!=1) {{mexErrMsgIdAndTxt(\"MyToolbox:arrayProduct:notRowVector\",\"The input must be a row vector.\");\n}}\n\n"
    read_scalar_s = "double {} = mxGetScalar(prhs[{}]);\n"
    read_array_a = "double *{} = mxGetPr(prhs[{}]);\n"
    output_var = "plhs[{}] = mxCreateNumericMatrix(1, {}, mxDOUBLE_CLASS, mxREAL);\n"
    output_ptr = "double *{} = mxGetPr(plhs[{}]);\n"
    
    # Sort the input list;
    input_list = [config_dict["inputs"][j] for j in np.argsort([i["position"] for i in config_dict["inputs"]])]
    # Sort the output list;
    output_list = [config_dict["outputs"][j] for j in np.argsort([i["position"] for i in config_dict["outputs"]])]
    
    
    # Add the caster arguments;
    caster_args = ""
    for i in input_list + output_list:
        caster_args += "double *" + i["name"] + ", "
    matlab_interface_src = matlab_interface_prototype.replace("__INSERT_CASTER_ARGS__", caster_args[:-2])      
    
    # Cast the inputs to the appropriate type;
    input_cast_declaration = ""
    for i in input_list:
        if i["type"] == "array":
            input_cast_declaration += input_cast_a.format(i["class"], i["name"], i["class"], i["length"], i["class"], i["length"], i["name"], i["class"], i["name"])
        elif i["type"] == "scalar":
            input_cast_declaration += input_cast_s.format(i["class"], i["name"], i["class"], i["name"]);
    matlab_interface_src = matlab_interface_src.replace("__INSERT_INPUT_CAST__\n", input_cast_declaration)         
            
    # Initialize the outputs to the appropriate types;
    output_alloc_declaration = ""
    for o in output_list:
        if o["type"] == "array":
            output_alloc_declaration += output_alloc_a.format(o["class"], o["name"], o["class"], o["length"], o["class"]);
        elif o["type"] == "scalar":
            output_alloc_declaration += output_alloc_s.format(o["class"], o["name"], o["class"], o["class"]);
    matlab_interface_src = matlab_interface_src.replace("__INSERT_OUTPUT_ALLOC__\n", output_alloc_declaration)      
        
    # Call the FPGA;
    fifo_call = ""
    for i in input_list + output_list:
        fifo_call += i["name"] + "_cast, "
    matlab_interface_src = matlab_interface_src.replace("__INSERT_FIFO_PARAMS__", fifo_call[:-2])   
    
    # Store the outputs;
    output_cast = ""
    for o in output_list:
        if o["type"] == "array":
            output_cast += output_cast_a.format(o["length"], o["name"], o["name"])
        elif o["type"] == "scalar":
            output_cast += output_cast_s.format(o["name"], o["name"])
    matlab_interface_src = matlab_interface_src.replace("__INSERT_OUTPUT_CAST__\n", output_cast)

    # Free the memory;
    free_mem = ""
    for i in input_list + output_list:
        if i["type"] == "array":
            free_mem += "free({}_cast);\n".format(i["name"])
    matlab_interface_src = matlab_interface_src.replace("__INSERT_FREE_MEM__\n", free_mem)
    
    
    # Check arguments number;
    matlab_interface_src = matlab_interface_src.replace("__INSERT_INPUT_NUM__", str(len(input_list)))
    matlab_interface_src = matlab_interface_src.replace("__INSERT_OUTPUT_NUM__", str(len(output_list)))
    
    # Check the type of scalar inputs;
    scalar_input_check = ""
    for i_i, i in enumerate(input_list):
        if i["type"] == "scalar":
            scalar_input_check += scalar_input_check_s.format(i_i, i_i, i_i)
    matlab_interface_src = matlab_interface_src.replace("__INSERT_SCALAR_TYPE_CHECK__\n", scalar_input_check)
    
    # Check the type of array inputs;
    array_input_check = ""
    for i_i, i in enumerate(input_list):
        if i["type"] == "array":
            array_input_check += array_input_check_a.format(i_i, i_i, i_i)
    matlab_interface_src = matlab_interface_src.replace("__INSERT_ARRAY_TYPE_CHECK__\n", array_input_check[:-2])
    
    # Read the scalar arguments;
    read_scalar = ""
    for i in input_list:
        if i["type"] == "scalar":
            read_scalar += read_scalar_s.format(i["name"], i["position"])
    matlab_interface_src = matlab_interface_src.replace("__INSERT_READ_SCALAR__\n", read_scalar)   
    
    # Read the array arguments;
    read_array = ""
    for i in input_list:
        if i["type"] == "array":
            read_array += read_array_a.format(i["name"], i["position"])
    matlab_interface_src = matlab_interface_src.replace("__INSERT_READ_ARRAY__\n", read_array)   
    
    # Allocate the outputs;
    output_var_declaration = ""
    for o_i, o in enumerate(output_list):
        output_var_declaration += output_var.format(o_i, (o["length"] if o["type"] == "array" else 1))
    matlab_interface_src = matlab_interface_src.replace("__INSERT_ALLOC_OUTPUT_VAR__\n", output_var_declaration) 
    
    # Get the output pointers;
    output_ptr_declaration = ""
    for o_i, o in enumerate(output_list):
        output_ptr_declaration += output_ptr.format(o["name"], o_i)
    matlab_interface_src = matlab_interface_src.replace("__INSERT_GET_OUTPUT_PTR__\n", output_ptr_declaration)    
    
    # Call the caster;
    caster_call = ""
    for i in input_list:
        if i["type"] == "array":
            caster_call += i["name"] + ", "
        elif i["type"] == "scalar":
            caster_call += "&" + i["name"] + ", "
    for o in output_list:
        caster_call += o["name"] + ", "
    matlab_interface_src = matlab_interface_src.replace("__INSERT_CASTER_CALL__", caster_call[:-2])    
    
    # Insert the kernel name;
    matlab_interface_src = matlab_interface_src.replace("__INSERT_KERNEL_NAME__", config_dict["kernel_name"])    
	
    return matlab_interface_src


if __name__ == "__main__":
    
    # Used to parse the input arguments;
    parser = argparse.ArgumentParser(description="Build the MATLAB interface that calls the FPGA.")
    parser.add_argument("-i", "--config", metavar="<path/to/kernel/config>",
                        help="Path to the a JSON file that describes the kernel for which the interface is built.")
    parser.add_argument("-p", "--prototype", metavar="<path/to/the/matlab/interface/prototype>",
                        help="Path to the C++ file used as prototype to build the MATLAB interface.")
    parser.add_argument("-o", "--output", metavar="<path/to/the/MATLAB/interface/file>",
                        help="Path to where the MATLAB interface file will be written.")   
    # Parse the input arguments;
#    args = parser.parse_args(["-i", "../../examples/var/src/config.json",
#                              "-p", "prototype_interface.cpp",
#                              "-o", "interface.cpp",
#                              ])
    args = parser.parse_args()
    
    config_path = args.config
    prototype_path = args.prototype
    matlab_interface_path = args.output
    
    
    # Read the JSON;
    with open(config_path, "r") as f:
        config_dict = json.loads(f.read())
        
    # Read the prototype host;
    with open(prototype_path, "r") as f:
        matlab_interface_prototype = f.read()
        
    # Build the host source code;  
    matlab_interface_src = build_matlab_interface(config_dict, matlab_interface_prototype)
    
    # Save the source code;
    with open(matlab_interface_path, "w+") as f:
        f.write(matlab_interface_src)
        
    # Format the code;
    subprocess.run(["astyle", "--style=allman", "--suffix=none", matlab_interface_path])
    

