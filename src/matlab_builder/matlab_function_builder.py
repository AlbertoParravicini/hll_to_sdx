import json
import argparse
import numpy as np


def build_matlab_function(config_dict, matlab_function_prototype):
    
    # Sort the input list;
    input_list = [config_dict["inputs"][j] for j in np.argsort([i["position"] for i in config_dict["inputs"]])]
    input_declaration = ""
    for i in input_list:
        input_declaration += i["name"] + ", "
    matlab_function_src = matlab_function_prototype.replace("__INSERT_INPUTS__", input_declaration[:-2])         
            
    # Sort the output list;
    output_list = [config_dict["outputs"][j] for j in np.argsort([i["position"] for i in config_dict["outputs"]])]
    output_declaration = ""
    for o in output_list:
        output_declaration += o["name"] + ", "
    matlab_function_src = matlab_function_src.replace("__INSERT_OUTPUTS__", output_declaration[:-2])    

    # Specify the function name;
    matlab_function_src = matlab_function_src.replace("__INSERT_FUNCTION_NAME__", config_dict["kernel_name"])
        
    return matlab_function_src


if __name__ == "__main__":
    
    # Used to parse the input arguments;
    parser = argparse.ArgumentParser(description="Build the MATLAB function that calls the FPGA.")
    parser.add_argument("-i", "--config", metavar="<path/to/kernel/config>",\
                        help="Path to the a JSON file that describes the kernel for which the interface is built.")
    parser.add_argument("-p", "--prototype", metavar="<path/to/the/matlab/function/prototype>",\
                        help="Path to the C++ file used as prototype to build the MATLAB function.")
    parser.add_argument("-o", "--output", metavar="<path/to/the/MATLAB/function/file>",\
                        help="Path to where the MATLAB function file will be written.")   
    # Parse the input arguments;
#    args = parser.parse_args(["-i", "../../examples/var/src/config.json",
#                              "-p", "prototype_function.m",
#                              "-o", "function.m",
#                              ])
    args = parser.parse_args()
    
    config_path = args.config
    prototype_path = args.prototype
    matlab_function_path = args.output
    
    
    # Read the JSON;
    with open(config_path, "r") as f:
        config_dict = json.loads(f.read())
        
    # Read the prototype host;
    with open(prototype_path, "r") as f:
        matlab_function_prototype = f.read()
        
    # Build the host source code;  
    matlab_function_src = build_matlab_function(config_dict, matlab_function_prototype)
    
    # Save the source code;
    with open(matlab_function_path, "w+") as f:
        f.write(matlab_function_src)
    
    