function [__INSERT_OUTPUTS__] = __INSERT_FUNCTION_NAME___fpga(__INSERT_INPUTS__)
    mex -v COMPFLAGS='$COMPFLAGS -rtl' matlab_interface.cpp
    [__INSERT_OUTPUTS__] = matlab_interface(__INSERT_INPUTS__);
end
