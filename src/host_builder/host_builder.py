import json
import subprocess
import argparse

def build_host(config_dict, host_prototype):
    
    cl_fifo_input_a = "{} = ({} *) calloc({}, sizeof({}));\nres_trans = read(fifo_in, {}, sizeof({}) * {});\n"
    cl_fifo_input_s = "res_trans = read(fifo_in, &{}, sizeof({}));\n"
    cl_fifo_output_alloc_a = "{} = ({} *) calloc({}, sizeof({}));\n"
    cl_fifo_output_a = "res_trans = write(fifo_out, {}, sizeof({}) * {});\n"
    cl_fifo_output_s = "res_trans = write(fifo_out, &{}, sizeof({}));\n"
    cl_create_buffer_a = "{} = clCreateBuffer(context, {}, sizeof({}) * {}, NULL, NULL);\n"
    cl_create_buffer_s = "{} = clCreateBuffer(context, {}, sizeof({}), NULL, NULL);\n"
    cl_buffer_error = "if (!{})\n{{\nprintf(\"Error: Failed to allocate device memory!\\n\");\nreturn EXIT_FAILURE;\n}}\n"
    cl_enqueue_buffer_a = "err = clEnqueueWriteBuffer(commands, {}, CL_TRUE, 0, sizeof({}) * {}, {}, 0, NULL, NULL);\nif (err != CL_SUCCESS)\n\
        {{\nprintf(\"Error: Failed to write to queue!\\n\");\nreturn EXIT_FAILURE;\n}}"
    cl_enqueue_buffer_s = "err = clEnqueueWriteBuffer(commands, {}, CL_TRUE, 0, sizeof({}), &{}, 0, NULL, NULL);\nif (err != CL_SUCCESS)\n\
        {{\nprintf(\"Error: Failed to write to queue!\\n\");\nreturn EXIT_FAILURE;\n}}"
    cl_kernel_argument = "err = clSetKernelArg(kernel, {}, sizeof(cl_mem), &{});\n"
    cl_read_a = 	"err = clEnqueueReadBuffer(commands, {}, CL_TRUE, 0, sizeof({}) * {}, {}, 0, NULL, &readevent);"
    cl_read_s = 	"err = clEnqueueReadBuffer(commands, {}, CL_TRUE, 0, sizeof({}), &{}, 0, NULL, &readevent);"
    cl_release_mem = "clReleaseMemObject({});\n"
    
    # Insert boards list;
    host_src = host_prototype.replace("__INSERT_TARGET_DEVICES__", "\"" + "\", \"".join(config_dict["board"]) + "\"")
    
    # Insert xclbin list;
    host_src = host_src.replace("__INSERT_XCLBIN_FILES__", "\"" + "\", \"".join(config_dict["xclbin"]) + "\"")
    
    # Build inputs and outputs;
    input_declaration = ""
    for i in config_dict["inputs"]:
        if i["type"] == "array":
            input_declaration += i["class"] + " *" + i["name"] + ";\n"
        elif i["type"] == "scalar":
            input_declaration += i["class"] + " " + i["name"] + ";\n"
            
    input_cl_declaration = ""
    for i in config_dict["inputs"]:
        input_cl_declaration += "cl_mem " + i["name"] + "_cl;\n"
        
    output_declaration = ""
    for o in config_dict["outputs"]:
        if o["type"] == "array":
            output_declaration += o["class"] + " *" + o["name"] + ";\n"
        elif o["type"] == "scalar":
            output_declaration += o["class"] + " " + o["name"] + ";\n"
            
    output_cl_declaration = ""
    for o in config_dict["outputs"]:
        output_cl_declaration += "cl_mem " + o["name"] + "_cl;\n"       
      
    # Insert inputs;
    host_src = host_src.replace("__INSERT_INPUTS__\n", input_declaration)
    # Insert outputs;
    host_src = host_src.replace("__INSERT_OUTPUTS__\n", output_declaration)
    # Insert OpenCL inputs;
    host_src = host_src.replace("__INSERT_CL_INPUTS__\n", input_cl_declaration)
    # Insert OpenCL outputs;
    host_src = host_src.replace("__INSERT_CL_OUTPUTS__\n", output_cl_declaration)   
    
    # Handle the loop;
    loop_present = config_dict["num_iterations"] > 1
    infinite_loop = config_dict["num_iterations"] < 1
    loop_start = ""
    if loop_present:
        loop_start = "int num_iteration = 0;\nconst int max_iterations = {};\n\
        for (num_iteration = 0; num_iteration < max_iterations; num_iteration++)\n{{\n\
        printf(\"--> Iteration number: %d\\n\", num_iteration);".format(str(config_dict["num_iterations"]))
    if infinite_loop:
        loop_start = "while (true)\n{"
    host_src = host_src.replace("__INSERT_LOOP_START__", loop_start)   
    
    # Allocate and read FIFO inputs;
    fifo_inputs = ""
    for i in config_dict["inputs"]:
        if i["type"] == "array":
            fifo_inputs += cl_fifo_input_a.format(i["name"], i["class"], i["length"], i["class"], i["name"], i["class"], i["length"])
        elif i["type"] == "scalar":
            fifo_inputs += cl_fifo_input_s.format(i["name"], i["class"])
    host_src = host_src.replace("__INSERT_FIFO_INPUT__\n", fifo_inputs)
    
    # Allocate FIFO outputs;
    fifo_outputs = ""
    for o in config_dict["outputs"]:
        if o["type"] == "array":
            fifo_outputs += cl_fifo_output_alloc_a.format(o["name"], o["class"], o["length"], i["class"])
    host_src = host_src.replace("__INSERT_FIFO_OUTPUT_ALLOC__\n", fifo_outputs)
          
    # Insert kernel name to load the binary;
    host_src = host_src.replace("__INSERT_KERNEL_NAME__", '"' + config_dict["kernel_name"] + '"')
	
	# Insert kernel name to save the execution times;
    host_src = host_src.replace("__INSERT_KERNEL_NAME_T__", config_dict["kernel_name"])
    
    # Build buffers;
    buffers = ""
    for i in config_dict["inputs"]:
        if i["type"] == "array":
            buffers += cl_create_buffer_a.format(i["name"] + "_cl", "CL_MEM_READ_ONLY", i["class"], i["length"])
        elif i["type"] == "scalar":
            buffers += cl_create_buffer_s.format(i["name"] + "_cl", "CL_MEM_READ_ONLY", i["class"])
        buffers += cl_buffer_error.format(i["name"] + "_cl") 
        
    for i in config_dict["outputs"]:
        if i["type"] == "array":
            buffers += cl_create_buffer_a.format(i["name"] + "_cl", "CL_MEM_WRITE_ONLY", i["class"], i["length"])
        elif i["type"] == "scalar":
            buffers += cl_create_buffer_s.format(i["name"] + "_cl", "CL_MEM_WRITE_ONLY", i["class"])
        buffers += cl_buffer_error.format(i["name"] + "_cl") 
    # Insert OpenCL buffers;
    host_src = host_src.replace("__INSERT_CL_BUFFERS__\n", buffers)
    
    # Enqueue input buffers;
    buffers = ""
    for i in config_dict["inputs"]:
        if i["type"] == "array":
            buffers += cl_enqueue_buffer_a.format(i["name"] + "_cl", i["class"], i["length"], i["name"])
        elif i["type"] == "scalar":
            buffers += cl_enqueue_buffer_s.format(i["name"] + "_cl", i["class"], i["name"])
    host_src = host_src.replace("__INSERT_ENQUEUE_BUFFERS__\n", buffers)    
    
    # Insert kernel arguments;
    arguments = ""
    for i in config_dict["inputs"] + config_dict["outputs"]:
        arguments += cl_kernel_argument.format(i["position"], i["name"] + "_cl")
    host_src = host_src.replace("__INSERT_KERNEL_ARGUMENTS__\n", arguments)
    
    # Read output buffers;
    buffers = ""
    for o in config_dict["outputs"]:
        if o["type"] == "array":
            buffers += cl_read_a.format(o["name"] + "_cl", o["class"], o["length"], o["name"])
        elif o["type"] == "scalar":
            buffers += cl_read_s.format(o["name"] + "_cl", o["class"], o["name"])
    host_src = host_src.replace("__INSERT_READ_OUTPUT__\n", buffers)
    
    # Release OpenCL memory;
    release_mem = ""
    for i in config_dict["inputs"] + config_dict["outputs"]:
        release_mem += cl_release_mem.format(i["name"] + "_cl")
    host_src = host_src.replace("__INSERT_RELEASE_MEMORY__\n", release_mem)  
    
    # Read FIFO outputs;
    fifo_outputs = ""
    for o in config_dict["outputs"]:
        if o["type"] == "array":
            fifo_outputs += cl_fifo_output_a.format(o["name"], o["class"], o["length"])
        elif i["type"] == "scalar":
            fifo_outputs += cl_fifo_output_s.format(o["name"], o["class"])
    host_src = host_src.replace("__INSERT_FIFO_OUTPUT__\n", fifo_outputs)
    
    # Free memory occupied by buffers;
    free_mem = ""
    for i in config_dict["inputs"] + config_dict["outputs"]:
        if i["type"] == "array":
            free_mem += "free({});\n".format(i["name"]);
    host_src = host_src.replace("__INSERT_FREE_MEMORY__\n", free_mem)
    
    # Handle loop end;
    loop_end = ""
    if loop_present or infinite_loop:
        loop_end = "}"
    host_src = host_src.replace("__INSERT_CLOSE_LOOP__", loop_end)
    
    return host_src

if __name__ == "__main__":
    
    # Used to parse the input arguments;
    parser = argparse.ArgumentParser(description="Build an OpenCL host file for a kernel described with an input configuration file.")
    parser.add_argument("-i", "--config", metavar="<path/to/kernel/config>",\
                        help="Path to the a JSON file that describes the kernel for which the host is built.")
    parser.add_argument("-p", "--prototype", metavar="<path/to/the/host/prototype>",\
                        help="Path to the C++ file used as prototype to build the host.")
    parser.add_argument("-o", "--output", metavar="<path/to/the/host/file>",\
                        help="Path to where the host file will be written.")   
    # Parse the input arguments;
#    args = parser.parse_args(["-i", "../../examples/mmult/src/config.json",
#                              "-p", "prototype.cpp",
#                              "-o", "output.cpp",
#                              ])
    args = parser.parse_args()
    
    config_path = args.config
    prototype_path = args.prototype
    host_path = args.output
   
    
    # Read the JSON;
    with open(config_path, "r") as f:
        config_dict = json.loads(f.read())
        
    # Read the prototype host;
    with open(prototype_path, "r") as f:
        host_prototype = f.read()
        
    # Build the host source code;  
    host_src = build_host(config_dict, host_prototype)
    
    # Save the source code;
    with open(host_path, "w+") as f:
        f.write(host_src)
    
    # Format the code;
    subprocess.run(["astyle", "--style=allman", "--suffix=none", host_path])
    
    
